package com.odigeo.membership.member.rest;


import com.odigeo.bookingapi.client.v14.configuration.BookingApiSecurityInterceptor;
import com.odigeo.bookingapi.client.v14.configuration.DateConverter;
import com.odigeo.bookingapi.v14.BookingApiService;
import com.odigeo.bookingsearchapi.v1.BookingSearchApiService;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.configuration.InterceptorConfiguration;
import com.odigeo.commons.rest.error.JsonIgnorePropertiesContextResolver;
import com.odigeo.commons.rest.error.SimpleRestErrorsHandler;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;

public final class ServiceConfigurationBuilder {

    private static final int MAX_CONCURRENT_CONNECTIONS = 20;
    private static final int BOOKINGAPI_TIMEOUT_MS = 15000;
    private static final int BOOKINGSEARCHAPI_TIMEOUT_MS = 15000;
    private static final int DEFAULT_SOCKET_TIMEOUT_MS = 5000;
    private static final int DEFAULT_CONNECTION_TIMEOUT_MS = 5000;

    private ServiceConfigurationBuilder() {
    }

    public static <T> ServiceConfiguration<T> setUpDefault(Class<T> serviceClass) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfiguration(DEFAULT_SOCKET_TIMEOUT_MS, DEFAULT_CONNECTION_TIMEOUT_MS);
        ServiceConfiguration<T> serviceConfiguration = new ServiceConfiguration.Builder<>(serviceClass)
                .withInterceptorConfiguration(new InterceptorConfiguration<>())
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(serviceClass))
                .build();
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }

    public static ServiceConfiguration setUpBookingApi(Class<BookingApiService> bookingApiServiceClass) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfiguration(BOOKINGAPI_TIMEOUT_MS, BOOKINGAPI_TIMEOUT_MS);
        InterceptorConfiguration<BookingApiService> interceptorConfiguration = new InterceptorConfiguration<>();
        interceptorConfiguration.addInterceptor(new BookingApiSecurityInterceptor());
        ServiceConfiguration<BookingApiService> serviceConfiguration = new ServiceConfiguration.Builder<>(bookingApiServiceClass)
                .withInterceptorConfiguration(interceptorConfiguration)
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(bookingApiServiceClass))
                .build();
        serviceConfiguration.getFactory().addStringConverter(DateConverter.class);
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());

        return serviceConfiguration;
    }

    private static ConnectionConfiguration getConnectionConfiguration(int socketTimeout, int connectionTimeout) {
        return new ConnectionConfiguration.Builder()
                .socketTimeoutInMillis(socketTimeout)
                .connectionTimeoutInMillis(connectionTimeout)
                .maxConcurrentConnections(MAX_CONCURRENT_CONNECTIONS)
                .build();
    }

    public static ServiceConfiguration setUpBookingSearchApi(Class<BookingSearchApiService> bookingSearchApiServiceClass) {
        ConnectionConfiguration connectionConfiguration = getConnectionConfiguration(BOOKINGSEARCHAPI_TIMEOUT_MS, BOOKINGSEARCHAPI_TIMEOUT_MS);
        InterceptorConfiguration<BookingSearchApiService> interceptorConfiguration = new InterceptorConfiguration<>();
        interceptorConfiguration.addInterceptor(new BookingApiSecurityInterceptor());
        ServiceConfiguration<BookingSearchApiService> serviceConfiguration = new ServiceConfiguration.Builder<>(bookingSearchApiServiceClass)
                .withInterceptorConfiguration(interceptorConfiguration)
                .withConnectionConfiguration(connectionConfiguration)
                .withRestErrorsHandler(new SimpleRestErrorsHandler(bookingSearchApiServiceClass))
                .build();
        serviceConfiguration.getFactory().addStringConverter(DateConverter.class);
        JsonIgnorePropertiesContextResolver.configureIntoFactory(serviceConfiguration.getFactory());
        return serviceConfiguration;
    }
}
