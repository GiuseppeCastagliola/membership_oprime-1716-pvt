package com.odigeo.membership.configuration.services;

import com.google.inject.Inject;
import com.odigeo.commons.monitoring.healthcheck.LiarDnsHealthCheck;
import com.odigeo.commons.monitoring.healthcheck.PingUrlFactory;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;

import java.net.MalformedURLException;
import java.net.URL;

public class UserProfileApiHealthChecker extends LiarDnsHealthCheck {

    @Inject
    public UserProfileApiHealthChecker(UserProfileApiSecurityConfiguration userProfileApiSecurityConfiguration) throws MalformedURLException {
        super(PingUrlFactory.getPingUrl(new URL(userProfileApiSecurityConfiguration.getPingUrl())));
    }
}
