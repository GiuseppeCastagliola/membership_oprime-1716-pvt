package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.exception.MembershipBadRequestException;
import com.odigeo.membership.parameters.UserCreation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang.StringUtils.isBlank;

public final class UserCreationRequestMapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCreationRequestMapper.class);
    private static final String USER_CREATION_INFO = "userCreationInfo";
    private static final String WEBSITE = "website";

    private UserCreationRequestMapper() {
    }

    public static UserCreation getMandatoryUserCreation(Map<String, String> requestFields) {
        UserCreation userCreation = null;
        String userCreationInfoJson = requestFields.get(USER_CREATION_INFO);
        if (nonNull(userCreationInfoJson)) {
            try {
                userCreation = UserCreation.fromString(userCreationInfoJson);
                userCreation.setWebsite(requestFields.get(WEBSITE));
            } catch (IOException e) {
                LOGGER.error("failed to read user creation details from {}", userCreationInfoJson);
                throw new MembershipBadRequestException("Invalid userCreationInfo details " + userCreationInfoJson, e);
            }
        }
        validate(userCreation);
        return userCreation;
    }

    private static void validate(UserCreation userCreation) throws MembershipBadRequestException {
        if (isNull(userCreation)) {
            throw new MembershipBadRequestException("User creation details not provided for membership creation");
        }
        if (isBlank(userCreation.getEmail())
                || isBlank(userCreation.getLocale())
                || isNull(userCreation.getTrafficInterfaceId())
                || isBlank(userCreation.getWebsite())) {
            throw new MembershipBadRequestException("Fields email, website, locale and traffic interface id are mandatory");
        }
    }
}
