package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.annotations.Authenticated;
import com.odigeo.membership.exception.MembershipUnauthorizedException;
import com.odigeo.membership.member.rest.utils.AuthenticationChecker;
import org.apache.commons.collections.CollectionUtils;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.core.ServerResponse;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.interception.AcceptedByMethod;
import org.jboss.resteasy.spi.interception.PreProcessInterceptor;

import javax.ws.rs.ext.Provider;
import java.lang.reflect.Method;
import java.util.List;

import static org.apache.commons.lang3.BooleanUtils.isFalse;

@Provider
public class MembershipAuthenticationInterceptor implements PreProcessInterceptor, AcceptedByMethod {

    private static final String CLIENT_HEADER = "client";
    private static final String HASH_HEADER = "hash";
    private static final String MODULE_NAME_PREFIX = "/membership";

    @Override
    public boolean accept(Class aClass, Method method) {
        return method.isAnnotationPresent(Authenticated.class);
    }

    @Override
    public ServerResponse preProcess(HttpRequest httpRequest, ResourceMethod resourceMethod) {
        String client = getHeader(httpRequest, CLIENT_HEADER);
        String hashedMessage = getHeader(httpRequest, HASH_HEADER);
        AuthenticationChecker authenticationChecker = ConfigurationEngine.getInstance(AuthenticationChecker.class);
        if (isFalse(authenticationChecker.check(client, hashedMessage, MODULE_NAME_PREFIX + httpRequest.getPreprocessedPath()))) {
            throw new MembershipUnauthorizedException("Client " + client + " not authorized");
        }
        return null;
    }

    private String getHeader(HttpRequest httpRequest, String headerName) {
        List<String> requestHeader = httpRequest.getHttpHeaders().getRequestHeader(headerName);
        if (CollectionUtils.isNotEmpty(requestHeader)) {
            return requestHeader.get(0);
        }
        throw new MembershipUnauthorizedException("Authentication header " + headerName + " not found");
    }
}
