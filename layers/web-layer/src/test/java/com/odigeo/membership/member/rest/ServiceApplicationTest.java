package com.odigeo.membership.member.rest;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import static com.edreams.configuration.ConfigurationEngine.init;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class ServiceApplicationTest {

    private static final int EXPECTED_SINGLETONS_SIZE = 15;

    @Mock
    private ServiceApplication serviceApplication;
    @Mock
    private ServletConfig servletConfig;
    @Mock
    private ServletContext servletContext;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        init();
        when(servletConfig.getServletContext()).thenReturn(servletContext);
    }

    @Test
    public void testGetSingletonsReturnsNullIfApplicationDidNotStart() {
        when(this.serviceApplication.getSingletons()).thenCallRealMethod();
        assertNull(this.serviceApplication.getSingletons());
    }

    @Test
    public void testConstructorReturnsSingletons() {
        assertEquals(new ServiceApplication(servletConfig).getSingletons().size(), EXPECTED_SINGLETONS_SIZE);
    }
}
