package com.odigeo.util;

import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.FeeLabel;
import com.odigeo.fees.model.FixFee;
import com.odigeo.membership.MembershipFee;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.enums.ProductType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FeeMapper {
    public static final String MEMBERSHIP_RENEWAL_FEE_SUBCODE = "AE10";

    private AbstractFee createAbstractFee(BigDecimal subscriptionPrice, String currencyCode) {
        FixFee fixFee = new FixFee();
        fixFee.setAmount(subscriptionPrice);
        fixFee.setSubCode(MEMBERSHIP_RENEWAL_FEE_SUBCODE);
        fixFee.setCurrency(Currency.getInstance(currencyCode));
        fixFee.setCreationDate(new Date());
        fixFee.setFeeLabel(FeeLabel.MARKUP_TAX);
        return fixFee;
    }

    public Fee abstractFeeToProductFee(AbstractFee abstractFee) {
        FixFee fixFee = (FixFee) abstractFee;
        Fee fee = new Fee();
        fee.setPrice(getPrice(fixFee.getAmount(), fixFee.getCurrency().getCurrencyCode()));
        fee.setCreationDate(fixFee.getCreationDate());
        fee.setLabel(fixFee.getFeeLabel().toString());
        fee.setSubCode(fixFee.getSubCode());
        return fee;
    }

    public MembershipFee abstractFeeToMembershipFee(AbstractFee abstractFee) {
        FixFee fixFee = (FixFee) abstractFee;
        MembershipFee membershipFee = new MembershipFee(null, fixFee.getAmount(), fixFee.getCurrency().getCurrencyCode(), ProductType.MEMBERSHIP_RENEWAL.toString());
        membershipFee.setSubCode(fixFee.getSubCode());
        return membershipFee;
    }

    public Price getPrice(BigDecimal amount, String currency) {
        Price price = new Price();
        price.setAmount(amount);
        price.setCurrency(Currency.getInstance(currency));
        return price;
    }

    public Map<FeeLabel, List<AbstractFee>> membershipFeeToFeeLabelMap(BigDecimal subscriptionPrice, String currencyCode) {
        Map<FeeLabel, List<AbstractFee>> feeLabelListHashMap = new HashMap<>();
        ArrayList<AbstractFee> abstractFees = new ArrayList<>();
        abstractFees.add(createAbstractFee(subscriptionPrice, currencyCode));
        feeLabelListHashMap.put(FeeLabel.MARKUP_TAX, abstractFees);
        return feeLabelListHashMap;
    }

}
