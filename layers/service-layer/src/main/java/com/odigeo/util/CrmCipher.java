package com.odigeo.util;

import com.odigeo.membership.util.EncryptUtils;
import com.odigeo.crm.CrmCipherConfiguration;
import org.apache.commons.lang.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;

@Singleton
public class CrmCipher {
    private static final String ALGORITHM = "DES/CBC/NoPadding";
    private static final String CRYPTOGRAPHY = "DES";
    private static final double PADDING_ORDER = 8.0;
    private static final String NUL_BYTE = "\0";
    private final CrmCipherConfiguration crmCipherConfiguration;
    private final EncryptUtils encryptUtils;

    @Inject
    public CrmCipher(CrmCipherConfiguration crmCipherConfiguration, EncryptUtils encryptUtils) {
        this.crmCipherConfiguration = crmCipherConfiguration;
        this.encryptUtils = encryptUtils;
    }

    private SecretKey getSecretKey() throws InvalidKeyException {
        DESKeySpec desKeySpec = new DESKeySpec(this.encryptUtils.getBytesUTF8(this.crmCipherConfiguration.getSecretKey()));
        return new SecretKeySpec(desKeySpec.getKey(), CRYPTOGRAPHY);
    }

    public String decryptToken(String token) throws UnsupportedEncodingException {
        byte[] decodedTokenBuffer = this.encryptUtils.decodeUrlTokenBuffer(token);
        byte[] decodedMsg = doCryptography(Cipher.DECRYPT_MODE, decodedTokenBuffer);
        return new String(decodedMsg, Charset.forName(StandardCharsets.UTF_8.name()))
                .replaceAll(NUL_BYTE, StringUtils.EMPTY)
                .trim();
    }

    public String encryptMessage(String message) throws UnsupportedEncodingException {
        byte[] encryptedMsg = doEncrypt(message);
        return this.encryptUtils.encodeUrlTokenBuffer(encryptedMsg);
    }

    private byte[] doEncrypt(String text) {
        byte[] notPaddedTextBuffer = this.encryptUtils.getBytesUTF8(text);
        byte[] paddedTextBuffer = new byte[(int) ((Math.ceil(notPaddedTextBuffer.length / PADDING_ORDER)) * PADDING_ORDER)];
        System.arraycopy(notPaddedTextBuffer, 0, paddedTextBuffer, 0, notPaddedTextBuffer.length);
        return doCryptography(Cipher.ENCRYPT_MODE, paddedTextBuffer);
    }

    private byte[] doCryptography(int encryptMode, byte[] buffer) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(encryptMode, getSecretKey(), new IvParameterSpec(getSecretKey().getEncoded()));
            return cipher.doFinal(buffer);
        } catch (GeneralSecurityException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
