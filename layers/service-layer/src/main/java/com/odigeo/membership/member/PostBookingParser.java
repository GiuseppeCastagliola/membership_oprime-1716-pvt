package com.odigeo.membership.member;

import com.edreams.base.MissingElementException;
import com.google.common.collect.ImmutableMap;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userprofiles.api.v1.model.TrafficInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

public final class PostBookingParser {

    public static final String TOKEN = ";";
    private static final String END_LINE = "\r?\n";
    public static final int DEFAULT_MONTHS_TO_RENEWAL = 6;
    public static final int FILE_ROW_OFFSET = 2;
    private static final String LOCALE_EN_GB = "en_GB";
    private static final String LOCALE_FR_FR = "fr_FR";
    private static final ImmutableMap<String, String> LOCALE_BY_WEBSITE = ImmutableMap.<String, String>builder()
            .put("IT", "it_IT").put("ES", "es_ES").put("FR", LOCALE_FR_FR).put("GOFR", LOCALE_FR_FR).put("OPFR", LOCALE_FR_FR)
            .put("DE", "de_DE").put("OPDE", "de_DE").put("UK", LOCALE_EN_GB).put("OPUK", LOCALE_EN_GB).put("US", "en_US")
            .put("PT", "pt_PT").put("GB", LOCALE_EN_GB).put("OPGB", LOCALE_EN_GB).put("OPCH", "fr_CH").put("AU", "en_AU")
            .build();

    private static final int NAME_INDEX = 0;
    private static final int LASTNAMES_INDEX = 1;
    private static final int WEBSITE_INDEX = 2;
    private static final int EMAIL_INDEX = 3;
    private static final String[] MANDATORY_ATTRIBUTES = {"Name", "LastNames", "Website", "Email"};

    private final List<String> lines;
    private final List<String> errors;

    public PostBookingParser(String csv) {
        this.errors = new ArrayList<>();
        lines = getLinesFromCsvAsString(csv);
    }

    private static List<String> getLinesFromCsvAsString(String csv) {
        if (Objects.nonNull(csv) && !csv.isEmpty()) {
            List<String> lines = Arrays.asList(csv.split(END_LINE));
            return lines.subList(1, lines.size());
        }
        return new ArrayList<>();
    }

    public String[] parseLine(String line) throws MissingElementException {
        String[] userInfo = line.split(TOKEN);
        int minLength = MANDATORY_ATTRIBUTES.length;
        if (userInfo.length < minLength) {
            throw new MissingElementException("Wrong format in the processed line");
        }
        return userInfo;
    }

    private static String getNonNumericalAttribute(int attribute, String... userInfo) throws MissingElementException {
        String value = userInfo[attribute].trim();
        if (value.isEmpty()) {
            throw new MissingElementException(MANDATORY_ATTRIBUTES[attribute] + " attribute empty");
        }
        return value;
    }

    public String getEmail(String... userInfo) throws MissingElementException {
        String email = getNonNumericalAttribute(EMAIL_INDEX, userInfo);
        if (Pattern.matches("(.+)@(.+)(\\.)(.+)", email)) {
            return email;
        }
        throw new MissingElementException("Wrong email format");
    }

    MembershipCreation getEmployeeMembershipCreation(String... userInfo) throws MissingElementException {
        String website = getNonNumericalAttribute(WEBSITE_INDEX, userInfo);
        return new MembershipCreationBuilder()
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                        .name(getNonNumericalAttribute(NAME_INDEX, userInfo))
                        .lastNames(getNonNumericalAttribute(LASTNAMES_INDEX, userInfo)))
                .withWebsite(website)
                .withMembershipType(MembershipType.EMPLOYEE)
                .withSourceType(SourceType.POST_BOOKING)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withUserCreation(new UserCreation.Builder()
                        .withEmail(getEmail(userInfo))
                        .withWebsite(website)
                        .withLocale(LOCALE_BY_WEBSITE.getOrDefault(website, LOCALE_EN_GB))
                        .withTrafficInterfaceId(TrafficInterface.OTHERS.getId())
                        .build())
                .build();
    }

    public List<String> getLines() {
        return lines;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void addError(String error) {
        errors.add(error);
    }
}
