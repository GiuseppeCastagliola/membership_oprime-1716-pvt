package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.ActivatedMembershipException;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearchBuilder;
import com.odigeo.membership.search.SearchService;
import org.apache.commons.collections.CollectionUtils;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public abstract class MembershipCreationFactory {

    public abstract Long createMembership(DataSource dataSource, MembershipCreation membershipCreation)
            throws MissingElementException, DataAccessException;

    void storeMemberStatusAction(DataSource dataSource, Long memberId, StatusAction action) throws DataAccessException {
        try {
            getMemberStatusActionStore().createMemberStatusAction(dataSource, memberId, action);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + action.toString() + " for membershipId " + memberId, e);
        }
    }

    void incrementCreationMetricsCounter(String metricName) {
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(metricName), MetricsNames.METRICS_REGISTRY_NAME);
    }

    private MemberStatusActionStore getMemberStatusActionStore() {
        return ConfigurationEngine.getInstance(MemberStatusActionStore.class);
    }

    void existActivatedMembershipForUserCheck(MembershipCreation membershipCreation, SearchService searchService) throws ActivatedMembershipException, DataAccessException {
        List<Membership> memberships = searchService.searchMemberships(new MembershipSearchBuilder()
                .status(MemberStatus.ACTIVATED.name())
                .website(membershipCreation.getWebsite())
                .withMemberAccount(Boolean.TRUE)
                .memberAccountSearch(new MemberAccountSearch.Builder()
                        .userId(membershipCreation.getMemberAccountCreation().getUserId())
                        .build())
                .build());
        if (CollectionUtils.isNotEmpty(memberships)) {
            throw new ActivatedMembershipException("An activated membership exists for userId " + membershipCreation.getMemberAccountCreation().getUserId() + " and website " + membershipCreation.getWebsite());
        }
    }
}
