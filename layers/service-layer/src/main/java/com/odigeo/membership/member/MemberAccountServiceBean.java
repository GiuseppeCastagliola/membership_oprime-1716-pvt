package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.UpdateMemberAccountOperation;
import com.odigeo.membership.member.update.MemberAccountUserIdUpdater;
import com.odigeo.membership.member.update.MembershipTransferMarketingHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.odigeo.membership.UpdateMemberAccountOperation.UPDATE_NAMES;
import static java.util.Objects.nonNull;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Stateless
@Local(MemberAccountService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MemberAccountServiceBean extends AbstractServiceBean implements MemberAccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberAccountServiceBean.class);
    private static final Predicate<MemberStatus> IS_ACTIVATED = status -> status.is(MemberStatus.ACTIVATED);
    private static final Predicate<MemberStatus> IS_PENDING_TO_COLLECT = status -> status.is(MemberStatus.PENDING_TO_COLLECT);
    private static final Predicate<Membership> IS_ACTIVE_OR_PTC = membership -> IS_ACTIVATED.or(IS_PENDING_TO_COLLECT).test(membership.getStatus());
    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;
    private MembershipTransferMarketingHandler membershipTransferMarketingHandler;

    @Override
    public MemberAccount getMembershipAccount(long userId, String website) throws DataAccessException {
        MemberAccount memberAccount = getMemberWithActivatedMemberships(userId, website);
        MemberServiceHelper.setBookingLimitReached(memberAccount);
        return memberAccount;
    }

    @Override
    public List<MemberAccount> getActiveMembersByUserId(long userId) throws DataAccessException {
        return getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource);
    }

    @Override
    public MemberAccount getMemberAccountByMembershipId(long membershipId) throws MissingElementException, DataAccessException {
        Membership membership = getMemberManager().getMembershipById(dataSource, membershipId);
        MemberAccount memberAccount = getMemberManager()
                .getMemberAccountsByMemberAccountId(membership.getMemberAccountId(), false, dataSource);
        memberAccount.setMemberships(Collections.singletonList(membership));
        return memberAccount;
    }

    @Override
    public MemberAccount getMemberWithActivatedMemberships(long userId, String website) throws DataAccessException {
        Optional<MemberAccount> memberAccount = getMemberManager()
                .getMembersWithActivatedMembershipsByUserId(userId, dataSource)
                .stream()
                .filter(MemberServiceHelper.websiteCheck(website))
                .findFirst();
        if (!memberAccount.isPresent()) {
            LOGGER.info("UserId {} doesn't have active prime subscription in {} ", userId, website);
        }
        return memberAccount.orElse(null);
    }

    @Override
    public Boolean updateMemberAccount(UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException {
        UpdateMemberAccountOperation updateMemberAccountOperation = Optional.ofNullable(updateMemberAccount.getOperation()).
                map(UpdateMemberAccountOperation::valueOf)
                .orElse(UPDATE_NAMES);
        switch (updateMemberAccountOperation) {
        case UPDATE_USER_ID:
            return updateMemberAccountUserId(updateMemberAccount);
        case UPDATE_NAMES:
            return updateMemberAccountNames(updateMemberAccount);
        default:
            throw new InvalidParameterException("Unknown operation parameter: " + updateMemberAccount.getOperation());
        }
    }

    @Override
    public Set<String> getActiveMembershipSitesByUserId(final long userId) throws DataAccessException {
        return getMemberManager().getMembersWithActivatedMembershipsByUserId(userId, dataSource).stream()
                .map(memberAccount -> memberAccount.getMemberships().get(0))
                .map(Membership::getWebsite)
                .map(String::toUpperCase)
                .collect(Collectors.toSet());
    }

    @Override
    public MemberAccount getMemberAccountById(final long accountId, boolean withMemberships) throws MissingElementException, DataAccessException {
        return getMemberManager().getMemberAccountsByMemberAccountId(accountId, withMemberships, dataSource);
    }

    private Boolean updateMemberAccountUserId(final UpdateMemberAccount updateMemberAccount) throws DataAccessException, MissingElementException {
        getMemberAccountUserIdUpdater().validateParametersForUpdateUserIdOperation(updateMemberAccount);
        final MemberAccount memberAccountWithMemberships = getMemberAccountById(
                Long.parseLong(updateMemberAccount.getMemberAccountId()), true);
        final long oldUserId = memberAccountWithMemberships.getUserId();
        final List<Membership> activeAndPtcMemberships = getActiveOrPtcMembership(memberAccountWithMemberships);
        boolean userIdUpdated = false;
        if (isNotEmpty(activeAndPtcMemberships)) {
            userIdUpdated = getMemberAccountUserIdUpdater().updateMemberAccountUserId(dataSource, updateMemberAccount);
            if (userIdUpdated) {
                getMembershipTransferMarketingHandler().subscribeNewUserAndUnsubscribeOldUser(
                        oldUserId, updateMemberAccount.getUserId(),
                        activeAndPtcMemberships, getActiveMembershipSitesByUserId(oldUserId));
            }
        }
        return userIdUpdated;
    }

    private List<Membership> getActiveOrPtcMembership(MemberAccount memberAccountWithMemberships) {
        return memberAccountWithMemberships.getMemberships().stream()
                .filter(IS_ACTIVE_OR_PTC)
                .collect(Collectors.toList());
    }

    private Boolean updateMemberAccountNames(UpdateMemberAccount updateMemberAccount) throws DataAccessException {
        if (nonNull(updateMemberAccount.getMemberAccountId()) && MemberServiceHelper.isValidPersonName(updateMemberAccount.getName())
                && MemberServiceHelper.isValidPersonName(updateMemberAccount.getLastNames())) {
            return getMemberManager().updateMemberAccountNames(dataSource, Long.valueOf(updateMemberAccount.getMemberAccountId()),
                    updateMemberAccount.getName(), updateMemberAccount.getLastNames());
        }
        return Boolean.FALSE;
    }

    private MemberAccountUserIdUpdater getMemberAccountUserIdUpdater() {
        return Optional.ofNullable(memberAccountUserIdUpdater).orElseGet(() -> {
            memberAccountUserIdUpdater = ConfigurationEngine.getInstance(MemberAccountUserIdUpdater.class);
            return memberAccountUserIdUpdater;
        });
    }

    private MembershipTransferMarketingHandler getMembershipTransferMarketingHandler() {
        return Optional.ofNullable(membershipTransferMarketingHandler).orElseGet(() -> {
            membershipTransferMarketingHandler = ConfigurationEngine.getInstance(MembershipTransferMarketingHandler.class);
            return membershipTransferMarketingHandler;
        });
    }
}
