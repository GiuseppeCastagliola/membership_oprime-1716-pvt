package com.odigeo.membership.tracking.autorenewal;

import com.odigeo.membership.AutorenewalTracking;

public interface AutorenewalTrackingService {
    void trackAutorenewalOperation(AutorenewalTracking autorenewalTracking);
}
