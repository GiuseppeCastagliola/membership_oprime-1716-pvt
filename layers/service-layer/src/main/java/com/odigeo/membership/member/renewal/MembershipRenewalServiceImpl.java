package com.odigeo.membership.member.renewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.product.MembershipSubscriptionConfiguration;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.LocalDateTime;

@Singleton
public class MembershipRenewalServiceImpl implements MembershipRenewalService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipRenewalServiceImpl.class);
    private static final int ONE = 1;

    private final MemberService memberService;
    private final MembershipStore membershipStore;
    private final MemberStatusActionStore memberStatusActionStore;
    private final MembershipMessageSendingManager membershipMessageSendingManager;
    private final MembershipSubscriptionConfiguration membershipSubscriptionConfiguration;

    @Inject
    public MembershipRenewalServiceImpl(MemberService memberService,
                                        MembershipStore membershipStore,
                                        MemberStatusActionStore memberStatusActionStore,
                                        MembershipMessageSendingManager membershipMessageSendingManager,
                                        MembershipSubscriptionConfiguration membershipSubscriptionConfiguration) {
        this.memberService = memberService;
        this.membershipStore = membershipStore;
        this.memberStatusActionStore = memberStatusActionStore;
        this.membershipMessageSendingManager = membershipMessageSendingManager;
        this.membershipSubscriptionConfiguration = membershipSubscriptionConfiguration;
    }

    @Override
    public boolean activatePendingToCollect(DataSource dataSource, Membership membership) throws DataAccessException {
        boolean activationSuccess = activateRenewedMembership(dataSource, membership);
        if (activationSuccess) {
            storeStatusActionForRenewedMembership(dataSource, membership);
            sendRenewalMessageToCRMTopic(membership.getId());
            membershipMessageSendingManager.sendMembershipIdToMembershipReporter(membership.getId());
        }
        return activationSuccess;
    }

    private boolean activateRenewedMembership(DataSource dataSource, Membership membership) throws DataAccessException {
        try {
            return (membershipSubscriptionConfiguration.getMonthlySubscriptionMarketsList()
                    .contains(membership.getWebsite()))
                    ? activateMonthlyMembershipSubscription(dataSource, membership)
                    : membershipStore.activatePendingToCollect(dataSource, membership);
        } catch (SQLException e) {
            throw new DataAccessException("Cannot activate PTC membership " + membership, e);
        }
    }

    private boolean activateMonthlyMembershipSubscription(DataSource dataSource, Membership membership) throws DataAccessException {
        final LocalDateTime calculatedNewExpirationDate = LocalDateTime.now().plusMonths(ONE);
        membership.setExpirationDate(calculatedNewExpirationDate);
        return activateRenewedMonthlyMembershipSubscription(dataSource, membership);
    }

    private boolean activateRenewedMonthlyMembershipSubscription(DataSource dataSource, Membership membership) throws DataAccessException {
        try {
            return membershipStore.activatePendingToCollectMonthlySubscription(dataSource, membership);
        } catch (SQLException e) {
            throw new DataAccessException("Cannot activate PTC membership " + membership, e);
        }
    }

    private void storeStatusActionForRenewedMembership(DataSource dataSource, Membership membershipRenewed) {
        try {
            storeMemberStatusAction(dataSource, membershipRenewed.getId());
        } catch (DataAccessException e) {
            LOGGER.error("Error creating status action for membershipId {}: ", membershipRenewed.getId(), e);
        }
    }

    private void storeMemberStatusAction(DataSource dataSource, Long memberId) throws DataAccessException {
        try {
            memberStatusActionStore.createMemberStatusAction(dataSource, memberId, StatusAction.ACTIVATION);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + StatusAction.ACTIVATION.toString() + " for membershipId " + memberId, e);
        }
    }

    private void sendRenewalMessageToCRMTopic(long membershipId) {
        LOGGER.info("Send subscription message for membershipId = {}", membershipId);
        try {
            final Membership updatedMembership = memberService.getMembershipByIdWithMemberAccount(membershipId);
            if (!membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(updatedMembership, SubscriptionStatus.SUBSCRIBED)) {
                LOGGER.warn("Error while trying to send message to CRM for membershipId: {}", membershipId);
                sendMetricsToGrafana();
            }
        } catch (MissingElementException | DataAccessException e) {
            LOGGER.error("Error retrieving membership and memberAccount for membershipId {}", membershipId);
        }
    }

    private void sendMetricsToGrafana() {
        MetricsUtils.incrementCounter(MetricsBuilder.buildFailedSendMessageToCRM(), MetricsNames.METRICS_REGISTRY_NAME);
    }
}
