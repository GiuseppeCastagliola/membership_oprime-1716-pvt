package com.odigeo.membership.member.userarea;

import com.odigeo.bookingapi.v14.responses.Money;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Optional;

final class MoneyOperation {

    private MoneyOperation() {
    }

    static Money moneySum(Money moneyA, Money moneyB) {
        moneyA.setAmount(Optional.ofNullable(moneyA.getAmount()).orElse(BigDecimal.ZERO)
                .add(Optional.ofNullable(moneyB.getAmount()).orElse(BigDecimal.ZERO)));
        return moneyA;
    }

    static Money buildMoney(BigDecimal amount, String currency) {
        Money money = new Money();
        money.setAmount(amount);
        money.setCurrency(currency);
        return money;
    }

    static Money zero() {
        return MoneyOperation.buildMoney(BigDecimal.ZERO, StringUtils.EMPTY);
    }
}
