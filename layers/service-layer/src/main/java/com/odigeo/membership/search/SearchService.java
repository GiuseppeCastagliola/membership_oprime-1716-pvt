package com.odigeo.membership.search;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.parameters.search.MembershipSearch;

import java.util.List;
import java.util.Optional;

public interface SearchService {
    List<MemberAccount> searchMemberAccounts(MemberAccountSearch memberAccountSearch) throws DataAccessException;

    List<Membership> searchMemberships(MembershipSearch membershipSearch) throws DataAccessException;

    Optional<Membership> getCurrentMembership(Long userId, String website) throws DataAccessException;
}
