package com.odigeo.membership.auth.usecases;

import com.odigeo.membership.auth.AuthenticationResponse;
import com.odigeo.membership.auth.exception.MembershipAuthServiceException;

public interface AuthenticationLogoutUseCase {
    AuthenticationResponse logout(String token) throws MembershipAuthServiceException;
}
