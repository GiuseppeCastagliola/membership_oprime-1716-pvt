package com.odigeo.messaging.kafka;

import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;

public interface MembershipKafkaSender {

    void sendMembershipSubscriptionMessageToKafka(MembershipSubscriptionMessage membershipSubscriptionMessage) throws PublishMessageException;

    void sendMembershipUpdateMessageToKafka(MembershipUpdateMessage membershipUpdateMessage);

    void sendMembershipMailerMessageToKafka(MembershipMailerMessage membershipMailerMessage, boolean force);
}
