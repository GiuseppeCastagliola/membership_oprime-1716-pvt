package com.odigeo.messaging.kafka.messagepublishers;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.MessagePublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Logger;

@Singleton
public class WelcomeToPrimePublisher {

    private static final Logger LOGGER = Logger.getLogger(WelcomeToPrimePublisher.class);

    @MessagePublisher(topic = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1")
    private KafkaPublisher<Message> publisher;

    public void publishMembershipMailerMessage(MembershipMailerMessage membershipMailerMessage, Callback callback) {
        try {
            publisher.publish(membershipMailerMessage, callback);
            LOGGER.info(String.format("WelcomeToPrime MembershipMailerMessage published - membershipId:%s, bookingId:%s",
                    membershipMailerMessage.getMembershipId(), membershipMailerMessage.getBookingId()));
        } catch (PublishMessageException e) {
            LOGGER.error(String.format("Error publishing WelcomeToPrime MembershipMailerMessage: membershipId:%s, bookingId:%s",
                    membershipMailerMessage.getMembershipId(), membershipMailerMessage.getBookingId()), e);
        }
    }
}
