package com.odigeo.messaging.kafka.messagepublishers;

import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.apache.kafka.clients.producer.Callback;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipSubscriptionPublisherTest {
    private static final String EMAIL = "email";
    private static final String WEBSITE = "website";
    private static final LocalDateTime ACTIVATION_DATE = LocalDateTime.now();
    private static final LocalDateTime EXPIRATION_DATE = ACTIVATION_DATE.plusMonths(6);

    @Mock
    private KafkaPublisher<Message> kafkaPublisher;

    @InjectMocks
    private MembershipSubscriptionPublisher membershipSubscriptionPublisher = new MembershipSubscriptionPublisher();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void publishMessageTest() throws PublishMessageException {
        membershipSubscriptionPublisher.publishMembershipSubscriptionMessage(sampleMembershipSubscriptionMessage(), getCallback());
        verify(kafkaPublisher).publish(any(MembershipSubscriptionMessage.class), any(Callback.class));
    }

    @Test(expectedExceptions = PublishMessageException.class)
    public void throwsPublishMessageException() throws PublishMessageException {
        doThrow(new PublishMessageException("PublishMessageException", new RuntimeException())).when(kafkaPublisher).publish(any(MembershipSubscriptionMessage.class), any());
        membershipSubscriptionPublisher.publishMembershipSubscriptionMessage(sampleMembershipSubscriptionMessage(), getCallback());
    }

    private MembershipSubscriptionMessage sampleMembershipSubscriptionMessage() {
        return new MembershipSubscriptionMessage.Builder(EMAIL, WEBSITE)
                .withSubscriptionStatus(SubscriptionStatus.SUBSCRIBED)
                .withDates(ACTIVATION_DATE.toLocalDate(), EXPIRATION_DATE.toLocalDate())
                .build();
    }

    private Callback getCallback() {
        return (recordMetadata, e) -> {
        };
    }
}
