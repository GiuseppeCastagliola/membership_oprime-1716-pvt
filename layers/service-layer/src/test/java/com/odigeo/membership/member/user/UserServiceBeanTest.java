package com.odigeo.membership.member.user;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v1.UserApiService;
import com.odigeo.userprofiles.api.v1.model.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v1.model.Status;
import com.odigeo.userprofiles.api.v1.model.ThirdAppRequest;
import com.odigeo.userprofiles.api.v1.model.User;
import com.odigeo.userprofiles.api.v1.model.UserRegisteredResponse;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class UserServiceBeanTest {

    private static final String BRAND = "ED";
    private static final String WEBSITE = "ES";
    private static final String LOCALE = "es_ES";
    private static final Integer TRAFFIC_INTERFACE_ID = 1;
    private static final String EMAIL = "kenny.mccormic@mail.com";

    @Mock
    private UserApiService userApiService;

    @Mock
    private UserProfileApiSecurityConfiguration userApiSecurityConfig;

    @InjectMocks
    private UserServiceBean userServiceBean = new UserServiceBean();

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @BeforeMethod
    public void setup() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(UserApiService.class).toInstance(userApiService);
        binder.bind(UserProfileApiSecurityConfiguration.class).toInstance(userApiSecurityConfig);
    }

    @Test
    public void saveNewUser() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserNotRegisteredResponse());
        when(userApiService.saveUser(any(User.class))).thenReturn(getUserWithId());

        userServiceBean.saveUser(getValidUserCreation());

        verify(userApiService).saveUser(userArgumentCaptor.capture());
        assertEquals(userArgumentCaptor.getValue().getEmail(), EMAIL);
        assertNotNull(userArgumentCaptor.getValue().getLogin());
        assertNull(userArgumentCaptor.getValue().getLogin().getPassword());
        assertEquals(userArgumentCaptor.getValue().getBrand().name(), BRAND);
        assertEquals(userArgumentCaptor.getValue().getLocale(), LOCALE);
        assertEquals(userArgumentCaptor.getValue().getWebsite(), WEBSITE);
        assertEquals(userArgumentCaptor.getValue().getTrafficInterface().getId(), TRAFFIC_INTERFACE_ID.intValue());
    }

    @Test
    public void saveUserWithExistingUserParams() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserRegisteredResponse());
        userServiceBean.saveUser(getValidUserCreation());
        verify(userApiService, never()).saveUser(any(User.class));
    }

    @Test(expectedExceptions = {DataAccessException.class}, expectedExceptionsMessageRegExp = ".*Problem checking isUserRegistered.*")
    public void saveUserWithExceptionInIsRegisteredCall() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenThrow(new InvalidCredentialsException("bad username or password"));
        userServiceBean.saveUser(getValidUserCreation());
    }

    @Test(expectedExceptions = {DataAccessException.class}, expectedExceptionsMessageRegExp = ".*Problem occurred saving user.*")
    public void saveUserWithExceptionInUserProfileSaveUser() throws Exception {
        when(userApiService.isUserRegistered(any(ThirdAppRequest.class))).thenReturn(getUserNotRegisteredResponse());
        when(userApiService.saveUser(any(User.class))).thenThrow(new InvalidCredentialsException("bad credentials"));
        userServiceBean.saveUser(getValidUserCreation());
    }

    private static User getUserWithId() {
        User user = new User();
        user.setId(17L);
        return user;
    }

    private static UserRegisteredResponse getUserNotRegisteredResponse() {
        UserRegisteredResponse response = new UserRegisteredResponse();
        response.setRegistered(false);
        return response;
    }

    private static UserRegisteredResponse getUserRegisteredResponse() {
        UserRegisteredResponse response = new UserRegisteredResponse();
        response.setRegistered(true);
        response.setUserStatus(Status.PENDING_LOGIN);
        response.setId(39L);
        return response;
    }

    private UserCreation getValidUserCreation() {
        return new UserCreation.Builder()
                .withLocale(LOCALE)
                .withEmail(EMAIL)
                .withWebsite(WEBSITE)
                .withTrafficInterfaceId(TRAFFIC_INTERFACE_ID)
                .build();
    }
}
