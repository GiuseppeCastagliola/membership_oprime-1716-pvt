package com.odigeo.membership.member.renewal;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.google.common.collect.ImmutableList;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.member.MemberService;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.member.MembershipStore;
import com.odigeo.membership.product.MembershipSubscriptionConfiguration;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipRenewalServiceImplTest {

    private static final long MEMBER_ID1 = 10L;
    private static final long MONTHLY_MEMBER_ID = 91L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MONTHLY_MEMBER_ACCOUNT_ID = 8L;
    private static final String SITE_ES = "ES";
    private static final int DEFAULT_DURATION = 12;
    private static final String MONTHLY_WEBSITE = "CH";
    private static final List<String> MONTHLY_SUBSCRIPTION_SITES = ImmutableList.of(MONTHLY_WEBSITE);
    private static final int ONCE = 1;

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberService memberService;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManager;
    @Mock
    private MembershipSubscriptionConfiguration membershipSubscriptionConfiguration;

    private MembershipRenewalService membershipRenewalService;

    private Membership pendingToCollectMembership = getMembership(MEMBER_ID1, SITE_ES, MemberStatus.PENDING_TO_COLLECT, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1);
    private Membership monthlyPendingToCollectMembership = new MembershipBuilder().setId(MONTHLY_MEMBER_ID)
            .setWebsite(MONTHLY_WEBSITE).setStatus(MemberStatus.PENDING_TO_COLLECT)
            .setMembershipRenewal(MembershipRenewal.ENABLED)
            .setExpirationDate(LocalDateTime.now())
            .setMemberAccountId(MONTHLY_MEMBER_ACCOUNT_ID)
            .build();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipRenewalService = new MembershipRenewalServiceImpl(memberService, membershipStore, memberStatusActionStore,
                membershipMessageSendingManager, membershipSubscriptionConfiguration);
        MetricsUtils.removeAllMetrics(MetricsNames.METRICS_REGISTRY_NAME);
    }

    @Test
    public void testActivatePendingToCollect_NotActivated() throws DataAccessException, SQLException {
        when(membershipStore.activatePendingToCollect(dataSource, pendingToCollectMembership)).thenReturn(false);
        assertFalse(membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership));
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingManager, never()).sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class));
        verify(membershipMessageSendingManager, never()).sendMembershipIdToMembershipReporter(anyLong());
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testActivatePendingToCollect_ActivationException() throws SQLException, DataAccessException {
        when(membershipStore.activatePendingToCollect(dataSource, pendingToCollectMembership)).thenThrow(new SQLException());
        membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership);
    }

    @Test
    public void testActivatePendingToCollectHappyPathWithSuccessfulMsgToCRM() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC(pendingToCollectMembership);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class)))
                .thenReturn(true);
        runAndValidatePendingToCollect(pendingToCollectMembership);
    }

    @Test
    public void testActivatePendingToCollectHappyPathWithFailedMsgToCRM() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC(pendingToCollectMembership);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class)))
                .thenReturn(false);
        assertTrue(membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership));
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, pendingToCollectMembership.getId(), StatusAction.ACTIVATION);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
        assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildFailedSendMessageToCRM(), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void testActivatePendingToCollect_StatusActionStoreException() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC(pendingToCollectMembership);
        doThrow(new SQLException()).when(memberStatusActionStore).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        assertTrue(membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership));
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
    }

    @Test
    public void testActivatePendingToCollect_FetchMembershipException() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC(pendingToCollectMembership);
        doThrow(new DataAccessException(""))
                .when(memberService).getMembershipByIdWithMemberAccount(pendingToCollectMembership.getId());
        assertTrue(membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership));
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, pendingToCollectMembership.getId(), StatusAction.ACTIVATION);
        verify(membershipMessageSendingManager, never()).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
    }

    private void initMocksForActivatePTC(Membership membership) throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.activatePendingToCollect(dataSource, membership)).thenReturn(true);
        when(memberService.getMembershipByIdWithMemberAccount(pendingToCollectMembership.getId())).thenReturn(membership);
        when(membershipStore.activatePendingToCollectMonthlySubscription(dataSource, membership)).thenReturn(true);
    }

    @Test
    public void testActivateMonthlyPendingToCollectHappyPathWithSuccessfulMsgToCRM() throws DataAccessException, SQLException, MissingElementException {
        initMocksForActivatePTC(monthlyPendingToCollectMembership);
        mockGetMembershipByIdWithMemberAccount(monthlyPendingToCollectMembership);
        when(membershipSubscriptionConfiguration.getMonthlySubscriptionMarketsList()).thenReturn(MONTHLY_SUBSCRIPTION_SITES);
        when(membershipStore.activatePendingToCollectMonthlySubscription(dataSource, monthlyPendingToCollectMembership)).thenReturn(true);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class)))
                .thenReturn(true);
        runAndValidatePendingToCollect(monthlyPendingToCollectMembership);
    }

    private void runAndValidatePendingToCollect(Membership pendingToCollectMembership) throws DataAccessException, SQLException {
        assertTrue(membershipRenewalService.activatePendingToCollect(dataSource, pendingToCollectMembership));
        verify(memberStatusActionStore).createMemberStatusAction(dataSource, pendingToCollectMembership.getId(), StatusAction.ACTIVATION);
        verify(membershipMessageSendingManager).sendSubscriptionMessageToCRMTopic(pendingToCollectMembership, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManager).sendMembershipIdToMembershipReporter(pendingToCollectMembership.getId());
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildFailedSendMessageToCRM(), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void testActivateMonthlyPTCWithMsgToCRMExpiration() throws DataAccessException, SQLException, MissingElementException {
        final Membership monthlyPendingToCollectMembershipWithDayBefore = new MembershipBuilder().setId(MONTHLY_MEMBER_ID)
                .setWebsite(MONTHLY_WEBSITE).setStatus(MemberStatus.PENDING_TO_COLLECT)
                .setMembershipRenewal(MembershipRenewal.ENABLED)
                .setExpirationDate(LocalDateTime.now().minusDays(ONCE))
                .setMemberAccountId(MONTHLY_MEMBER_ACCOUNT_ID)
                .build();
        initMocksForActivatePTC(monthlyPendingToCollectMembershipWithDayBefore);
        mockGetMembershipByIdWithMemberAccount(monthlyPendingToCollectMembershipWithDayBefore);
        when(membershipSubscriptionConfiguration.getMonthlySubscriptionMarketsList()).thenReturn(MONTHLY_SUBSCRIPTION_SITES);
        when(membershipStore.activatePendingToCollectMonthlySubscription(dataSource, monthlyPendingToCollectMembershipWithDayBefore)).thenReturn(true);
        when(membershipMessageSendingManager.sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class)))
                .thenReturn(true);
        runAndValidatePendingToCollect(monthlyPendingToCollectMembershipWithDayBefore);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testActivateMonthlyPendingToCollectActivationException() throws SQLException, DataAccessException {
        when(membershipSubscriptionConfiguration.getMonthlySubscriptionMarketsList()).thenReturn(MONTHLY_SUBSCRIPTION_SITES);
        when(membershipStore.activatePendingToCollectMonthlySubscription(dataSource, monthlyPendingToCollectMembership)).thenThrow(new SQLException());
        membershipRenewalService.activatePendingToCollect(dataSource, monthlyPendingToCollectMembership);
    }

    private void mockGetMembershipByIdWithMemberAccount(Membership expectedMembership) throws MissingElementException, DataAccessException {
        when(memberService.getMembershipByIdWithMemberAccount(expectedMembership.getId())).thenReturn(expectedMembership);
    }

    private static Membership getMembership(long memberId, String website, MemberStatus status, MembershipRenewal renewal, long memberAccountId) {
        MembershipBuilder builder = new MembershipBuilder().setId(memberId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(renewal).setActivationDate(LocalDateTime.now()).setMemberAccountId(memberAccountId);
        if (Arrays.asList(MemberStatus.ACTIVATED, MemberStatus.DEACTIVATED).contains(status)) {
            builder.setExpirationDate(LocalDateTime.now().plusMonths(DEFAULT_DURATION));
        }
        return builder.build();
    }
}