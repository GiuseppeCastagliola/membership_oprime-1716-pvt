package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.creation.BasicFreeMembershipCreation;
import com.odigeo.membership.member.creation.MembershipCreationFactoryProvider;
import com.odigeo.membership.member.creation.MembershipCreationService;
import com.odigeo.membership.member.creation.NewMembershipSubscriptionCreation;
import com.odigeo.membership.member.creation.PendingToCollectMembershipCreation;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingManager;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class MemberServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;
    @Mock
    private MembershipStore membershipStore;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private MembershipCreationFactoryProvider membershipCreationFactoryProviderMock;
    @Mock
    private NewMembershipSubscriptionCreation newMembershipSubscriptionCreationMock;
    @Mock
    private PendingToCollectMembershipCreation pendingToCollectMembershipCreationMock;
    @Mock
    private BasicFreeMembershipCreation basicFreeMembershipCreationMock;
    @Mock
    private MembershipMessageSendingManager membershipMessageSendingManagerMock;
    @Mock
    private UserService userService;
    @Mock
    private MembershipCreationService membershipCreationService;

    @InjectMocks
    private MemberServiceBean memberServiceBean = new MemberServiceBean();

    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final int DEFAULT_DURATION = 12;
    private static final MembershipType MEMBERSHIP_TYPE = MembershipType.BASIC;
    private static final long USER_ID = 123L;
    private static final long MEMBER_ID1 = 10L;
    private static final long MEMBER_ID2 = 11L;
    private static final long MEMBER_ID4 = 13L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MEMBER_ACCOUNT_ID2 = 2L;
    private static final long BOOKING_ID = 800L;
    private static final String WEBSITE1 = "ES";
    private static final String WEBSITE2 = "FR";
    private static final String MEMBER_NAME1 = "JOSE";
    private static final String MEMBER_SURNAME1 = "LUIS";
    private static final String TEST_EXCEPTION = "TestException";
    private static final String TEST_EMAIL = "test@edreams.com";

    private MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
    private Membership activeMembership1 = getMembership(MEMBER_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, null);
    private Membership inactiveMembership2 = getMembership(MEMBER_ID2, WEBSITE2, MemberStatus.DEACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID2, null);
    private Membership activeMembership4 = getMembership(MEMBER_ID4, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, null);
    private Membership pendingMemberWithAccount = getMembership(MEMBER_ID4, WEBSITE1, MemberStatus.PENDING_TO_ACTIVATE, MembershipRenewal.DISABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private Membership activeMembership1WithAccount = getMembership(MEMBER_ID1, WEBSITE1, MemberStatus.ACTIVATED, MembershipRenewal.ENABLED, MEMBER_ACCOUNT_ID1, memberAccount);
    private MembershipCreation membershipCreation = new MembershipCreationBuilder()
            .withMemberAccountCreationBuilder(MemberAccountCreation.builder()
                    .userId(USER_ID)
                    .lastNames(MEMBER_SURNAME1)
                    .name(MEMBER_NAME1))
            .withWebsite(WEBSITE1).withMembershipType(MEMBERSHIP_TYPE).withSourceType(SourceType.POST_BOOKING).withSubscriptionPrice(BigDecimal.TEN)
            .withMemberAccountId(MEMBER_ACCOUNT_ID1).build();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        when(membershipCreationFactoryProviderMock.getInstance(any(MembershipCreation.class))).thenCallRealMethod();
    }

    private void configure(Binder binder) {
        binder.bind(MemberAccountService.class).toInstance(memberAccountService);
        binder.bind(NewMembershipSubscriptionCreation.class).toInstance(newMembershipSubscriptionCreationMock);
        binder.bind(PendingToCollectMembershipCreation.class).toInstance(pendingToCollectMembershipCreationMock);
        binder.bind(BasicFreeMembershipCreation.class).toInstance(basicFreeMembershipCreationMock);
        binder.bind(MembershipMessageSendingManager.class).toInstance(membershipMessageSendingManagerMock);
        binder.bind(MembershipCreationService.class).toInstance(membershipCreationService);
    }

    @Test
    public void testGetMembershipsById() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBER_ID1)).thenReturn(activeMembership1);
        when(membershipStore.fetchMembershipById(dataSource, MEMBER_ID2)).thenReturn(inactiveMembership2);
        assertEquals(memberServiceBean.getMembershipById(MEMBER_ID1), activeMembership1);
        assertEquals(memberServiceBean.getMembershipById(MEMBER_ID2), inactiveMembership2);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByIdException() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBER_ID1)).thenThrow(new SQLException());
        memberServiceBean.getMembershipById(MEMBER_ID1);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMembershipsByIdNotFound() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipById(dataSource, MEMBER_ID1)).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        memberServiceBean.getMembershipById(MEMBER_ID1);
    }

    @Test
    public void testCreateMembershipNewSubscription() throws DataAccessException, MissingElementException {
        when(membershipCreationService.create(membershipCreation)).thenReturn(MEMBER_ID1);
        long newMembershipId = memberServiceBean.createMembership(membershipCreation);
        assertEquals(newMembershipId, MEMBER_ID1);
        verify(membershipCreationService).create(membershipCreation);
    }

    @Test
    public void testCreateMembershipPendingToCollect() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipPendingToCollect = new MembershipCreationBuilder()
                .withMemberStatus(MemberStatus.PENDING_TO_COLLECT)
                .withMemberAccountId(MEMBER_ACCOUNT_ID1)
                .build();
        when(membershipCreationService.create(createMembershipPendingToCollect)).thenReturn(MEMBER_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipPendingToCollect);
        assertEquals(newMembershipId, MEMBER_ID2);
    }

    @Test
    public void testCreateMembershipBasicFree() throws DataAccessException, MissingElementException {
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID))
                .build();
        when(membershipCreationService.create(eq(createMembershipBasicFree))).thenReturn(MEMBER_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBER_ID2);
        verifyZeroInteractions(userService);
    }

    @Test
    public void testCreateMembershipAndUserBasicFree() throws DataAccessException, MissingElementException {
        UserCreation userCreation = new UserCreation.Builder()
                .withEmail(TEST_EMAIL)
                .withLocale(Locale.ENGLISH.toString())
                .withTrafficInterfaceId(1)
                .build();
        MembershipCreation createMembershipBasicFree = new MembershipCreationBuilder()
                .withMembershipType(MembershipType.BASIC_FREE)
                .withMemberStatus(MemberStatus.ACTIVATED)
                .withUserCreation(userCreation)
                .build();
        when(membershipCreationService.create(eq(createMembershipBasicFree))).thenReturn(MEMBER_ID2);
        long newMembershipId = memberServiceBean.createMembership(createMembershipBasicFree);
        assertEquals(newMembershipId, MEMBER_ID2);
    }

    @Test
    public void testActivateMembership() throws DataAccessException, SQLException, MissingElementException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBER_ID1, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership1));
        assertTrue(memberServiceBean.activateMembership(MEMBER_ID1, BOOKING_ID, BigDecimal.TEN));
        verify(membershipCreationService).activate(MEMBER_ID1, BigDecimal.TEN);
    }

    @Test
    public void testNotActivateMembership() throws DataAccessException, SQLException, MissingElementException {
        when(membershipCreationService.activate(MEMBER_ID1, BigDecimal.TEN)).thenReturn(Optional.empty());
        assertFalse(memberServiceBean.activateMembership(MEMBER_ID1, BOOKING_ID, BigDecimal.TEN));
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testActivateMemberSQLException() throws DataAccessException, SQLException, MissingElementException {
        setUpActivateMembershipStoreResponseException();
        when(membershipCreationService.activate(MEMBER_ID1, BigDecimal.TEN)).thenThrow(new DataAccessException(""));
        memberServiceBean.activateMembership(MEMBER_ID1, BOOKING_ID, BigDecimal.TEN);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testGetMembershipsByAccountIdSQLException()
            throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenThrow(new SQLException());
        memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1);
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetMembershipsByAccountIdMissingElementException()
            throws SQLException, DataAccessException, MissingElementException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenThrow(new DataNotFoundException(StringUtils.EMPTY));
        memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1);
    }

    @Test
    public void testGetMembershipsByAccountId() throws MissingElementException, DataAccessException, SQLException {
        when(membershipStore.fetchMembershipByMemberAccountId(dataSource, MEMBER_ACCOUNT_ID1)).thenReturn(Collections.singletonList(activeMembership1));
        assertNotNull(memberServiceBean.getMembershipsByAccountId(MEMBER_ACCOUNT_ID1));
    }

    @Test
    public void testActivateMembership_SendCorrectIdToReporter() throws DataAccessException, SQLException, MissingElementException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBER_ID4, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership4));
        assertTrue(memberServiceBean.activateMembership(MEMBER_ID4, BOOKING_ID, BigDecimal.TEN));
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(MEMBER_ID4);
    }

    @Test
    public void testCreateMembership_SendCorrectIdToReporter() throws DataAccessException, MissingElementException {
        when(newMembershipSubscriptionCreationMock.createMembership(dataSource, membershipCreation)).thenReturn(MEMBER_ID1);
        long newMembershipId = memberServiceBean.createMembership(membershipCreation);
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(newMembershipId);
    }

    @Test
    public void testActivateMembershipSubscription() throws SQLException, DataAccessException, MissingElementException {
        setUpActivateMembershipStoreResponse(Boolean.TRUE);
        when(membershipCreationService.activate(MEMBER_ID4, BigDecimal.TEN)).thenReturn(Optional.of(activeMembership4));
        assertTrue(memberServiceBean.activateMembership(MEMBER_ID4, BOOKING_ID, BigDecimal.TEN));
        verify(membershipMessageSendingManagerMock).sendMembershipIdToMembershipReporter(MEMBER_ID4);
        verify(membershipMessageSendingManagerMock).sendSubscriptionMessageToCRMTopic(activeMembership4, SubscriptionStatus.SUBSCRIBED);
        verify(membershipMessageSendingManagerMock).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(any(Membership.class), eq(BOOKING_ID), eq(false));
    }

    @Test
    public void testActivateMembership_AlreadyHasAnActiveMembership() throws DataAccessException, SQLException, MissingElementException {
        // GIVEN
        when(membershipCreationService.activate(MEMBER_ID1, BigDecimal.TEN)).thenReturn(Optional.empty());
        // WHEN
        final Boolean isMembershipActivated = memberServiceBean.activateMembership(MEMBER_ID1, BOOKING_ID, BigDecimal.TEN);
        // THEN
        assertFalse(isMembershipActivated);
        verify(memberStatusActionStore, never()).createMemberStatusAction(any(DataSource.class), anyLong(), any(StatusAction.class));
        verify(membershipMessageSendingManagerMock, never()).sendMembershipIdToMembershipReporter(anyLong());
        verify(membershipMessageSendingManagerMock, never()).sendSubscriptionMessageToCRMTopic(any(Membership.class), any(SubscriptionStatus.class));
        verify(membershipMessageSendingManagerMock, never()).sendWelcomeToPrimeMessageToMembershipTransactionalTopic(any(Membership.class), anyLong(), eq(false));
    }

    private MemberAccount assembleMemberAcccountWithActiveMemberInES() {
        MemberAccount memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
        memberAccount.setMemberships(Collections.singletonList(activeMembership1));
        return memberAccount;
    }

    private void setUpActivateMembershipStoreResponse(final boolean isUpdated) throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipByIdWithMemberAccount(eq(dataSource), anyLong())).thenReturn(pendingMemberWithAccount).thenReturn(activeMembership1WithAccount);
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenReturn(isUpdated);
    }

    private void setUpActivateMembershipStoreResponseException() throws SQLException, DataNotFoundException {
        when(membershipStore.fetchMembershipById(any(DataSource.class), anyLong())).thenThrow(new SQLException(TEST_EXCEPTION));
        when(membershipStore.activateMember(any(DataSource.class), anyLong(), any(LocalDateTime.class), any(LocalDateTime.class), any(BigDecimal.class))).thenThrow(new SQLException(TEST_EXCEPTION));
    }

    private static Membership getMembership(long memberId, String website, MemberStatus status, MembershipRenewal renewal, long memberAccountId, MemberAccount memberAccount) {
        MembershipBuilder builder = new MembershipBuilder().setId(memberId).setWebsite(website).setStatus(status)
                .setMembershipRenewal(renewal).setActivationDate(NOW).setMemberAccountId(memberAccountId)
                .setSourceType(SourceType.FUNNEL_BOOKING).setMemberAccount(memberAccount).setMonthsDuration(DEFAULT_DURATION);
        if (MemberStatus.ACTIVATED == status) {
            builder.setExpirationDate(NOW.plusMonths(DEFAULT_DURATION));
        }
        return builder.build();
    }
}
