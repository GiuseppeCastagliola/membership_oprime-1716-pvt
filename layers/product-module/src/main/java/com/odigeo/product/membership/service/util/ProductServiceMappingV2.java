package com.odigeo.product.membership.service.util;

import com.odigeo.membership.Membership;
import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.model.Fee;
import com.odigeo.product.v2.model.Price;
import com.odigeo.product.v2.model.Product;
import com.odigeo.product.v2.model.UserInteractionNeeded;
import com.odigeo.product.v2.model.enums.CloseActionResult;
import com.odigeo.product.v2.model.enums.ProductType;
import com.odigeo.product.v2.model.enums.ProviderProductStatus;
import com.odigeo.product.v2.model.responses.CloseProviderProductResponse;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public final class ProductServiceMappingV2 {

    private ProductServiceMappingV2() {

    }


    public static Product asProduct(Membership membership, List<Fee> fees) {
        Product product = new Product();
        product.setId(String.valueOf(membership.getId()));
        product.setFees(fees);
        product.setMerchantPrice(getPrice(fees));
        product.setProviderProducts(Collections.emptyList());
        product.setSellingPrice(getPrice(fees));
        product.setStatus(membership.getProductStatus().name());
        product.setType(ProductType.MEMBERSHIP_RENEWAL.toString());
        product.setFeeContainerId(membership.getFeeContainerId());
        return product;
    }

    public static BookingApiMembershipInfo asBookingApiMembershipInfo(Membership membership, List<Fee> fees) {
        BookingApiMembershipInfo bookingApiMembershipInfo = new BookingApiMembershipInfo();
        bookingApiMembershipInfo.setRenewalDate(getDate(membership.getExpirationDate()));
        bookingApiMembershipInfo.setMembershipId(membership.getId());
        bookingApiMembershipInfo.setRenewal(Boolean.TRUE);
        bookingApiMembershipInfo.setCurrencyCode(getPrice(fees).getCurrency().getCurrencyCode());
        bookingApiMembershipInfo.setPrice(getPrice(fees).getAmount());
        bookingApiMembershipInfo.setFeeContainerId(membership.getFeeContainerId());
        return bookingApiMembershipInfo;
    }

    public static CloseProviderProductResponse asCloseProviderProductResponse(ProviderProductStatus status) {
        CloseProviderProductResponse closeProviderProductResponse = new CloseProviderProductResponse();
        closeProviderProductResponse.setCloseActionResult(status == ProviderProductStatus.CONTRACT ? CloseActionResult.OK : CloseActionResult.KO);
        closeProviderProductResponse.setProviderProductStatus(status);
        closeProviderProductResponse.setRollbackable(Boolean.FALSE);
        closeProviderProductResponse.setUserInteractionNeeded(new UserInteractionNeeded());

        return closeProviderProductResponse;
    }

    private static Date getDate(LocalDateTime dateToConvert) {
        if (dateToConvert != null) {
            return Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
        }
        return null;
    }

    public static Price getPrice(List<Fee> fees) {
        return fees.get(0).getPrice();
    }

}
