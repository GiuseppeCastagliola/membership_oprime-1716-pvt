package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Optional;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;

public class MembershipNoSQLManagerImplTest {

    private static final long MEMBERSHIP_ID = 8L;
    private static final String BOOKING_ID = "1991";
    private static final int SECONDS_TO_EXPIRE = 3600;
    private static final String ERROR = "error";

    @Mock
    private MembershipNoSQLRepository membershipNoSQLRepository;

    private MembershipNoSQLManagerImpl membershipNoSQLManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        membershipNoSQLManager = new MembershipNoSQLManagerImpl(membershipNoSQLRepository);
    }

    @Test
    public void testGetBookingIdFromCacheAlreadyStoredInCache() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID))).thenReturn(BOOKING_ID);
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(MEMBERSHIP_ID);
        assertEquals(bookingId, Optional.of(Long.valueOf(BOOKING_ID)));
    }

    @Test
    public void testGetBookingIdFromCacheNotStoredInCache() throws DataAccessException {
        when(membershipNoSQLRepository.get(String.valueOf(MEMBERSHIP_ID)))
                .thenThrow(new DataAccessException(ERROR));
        Optional<Long> bookingId = membershipNoSQLManager.getBookingIdFromCache(MEMBERSHIP_ID);
        assertEquals(bookingId, Optional.empty());
    }

    @Test
    public void testStoreInCache() throws DataAccessException {
        membershipNoSQLManager.storeInCache(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        verify(membershipNoSQLRepository).store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
    }

    @Test
    public void testStoreInCacheWithFailureWithTheConnection() throws DataAccessException {
        doThrow(new DataAccessException(ERROR)).when(membershipNoSQLRepository)
                .store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        membershipNoSQLManager.storeInCache(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
        verify(membershipNoSQLRepository).store(String.valueOf(MEMBERSHIP_ID), BOOKING_ID, SECONDS_TO_EXPIRE);
    }
}