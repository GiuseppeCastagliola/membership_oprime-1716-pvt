package com.odigeo.membership.redis;

import bean.test.BeanTest;

public class RedisClientConfigurationTest extends BeanTest<RedisClientConfiguration>  {

    private static final String HOST = "host";
    private static final int PORT = 6379;

    @Override
    protected RedisClientConfiguration getBean() {
        RedisClientConfiguration redisClientConfiguration = new RedisClientConfiguration();
        redisClientConfiguration.setHost(HOST);
        redisClientConfiguration.setPort(PORT);
        return redisClientConfiguration;
    }
}