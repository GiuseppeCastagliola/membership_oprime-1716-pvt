package com.odigeo.membership;

import bean.test.BeanTest;
import com.odigeo.bookingapi.v14.responses.Money;

import java.util.Collections;

public class MemberSubscriptionDetailsTest extends BeanTest<MemberSubscriptionDetails> {

    private static final Long MEMBERSHIP_ID = 1L;
    public static final Money TOTAL_SAVINGS = new Money();

    @Override
    protected MemberSubscriptionDetails getBean() {
        return new MemberSubscriptionDetails().setMembershipId(MEMBERSHIP_ID)
                .setPrimeBookingsInfo(Collections.EMPTY_LIST)
                .setTotalSavings(TOTAL_SAVINGS);
    }
}
