package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PostBookingPageInfoTest extends BeanTest<PostBookingPageInfo> {

    private static final long BOOKING_ID = 1234L;
    private static final String EMAIL = "user@email.com";

    @Override
    protected PostBookingPageInfo getBean() {
        return new PostBookingPageInfo();
    }

    @Test
    public void testPostBookingPageInfo() {
        PostBookingPageInfo postBookingPageInfo = new PostBookingPageInfo().setBookingId(BOOKING_ID).setEmail(EMAIL);
        PostBookingPageInfo postBookingPageInfo2 = new PostBookingPageInfo(BOOKING_ID, EMAIL);
        assertEquals(postBookingPageInfo, postBookingPageInfo2);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = PostBookingPageInfo.EXCEPTION_MESSAGE)
    public void testPostBookingPageInfoValidatePageInfoShouldThrowExceptionIfNull() {
        new PostBookingPageInfo(BOOKING_ID, null).validatePageInfo();
    }

    @Test
    public void postBookingPageInfoEqualsVerifierTest() {
        EqualsVerifier.forClass(PostBookingPageInfo.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}
