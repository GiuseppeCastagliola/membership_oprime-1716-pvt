package com.odigeo.membership.parameters.search;

import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.function.Function;

public interface Search {

    enum SqlKeywords {
        SELECT, AND, OR, FROM, WHERE, JOIN, LEFT_OUTER_JOIN;
        private static final Function<String, String> CENTER_PAD = textToCenter -> StringUtils.center(textToCenter, textToCenter.length() + 2);
        private static final String SPACE = " ";
        private static final String UNDERSCORE = "_";

        @Override
        public String toString() {
            return CENTER_PAD.apply(StringUtils.replace(name(), UNDERSCORE, SPACE));
        }
    }

    String getQueryString();

    List<Object> getValues();

    boolean isEmpty();
}
