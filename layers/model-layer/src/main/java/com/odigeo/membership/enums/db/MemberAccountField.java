package com.odigeo.membership.enums.db;

import java.util.Arrays;

import static com.odigeo.membership.enums.db.FieldName.TableAlias.MEMBER_ACCOUNT_ALIAS;
import static java.util.stream.Collectors.joining;

public enum MemberAccountField implements FieldName {
    ID, USER_ID, FIRST_NAME, LAST_NAME, TIMESTAMP;

    public static final String MEMBER_ACCOUNT_FIELDS = Arrays.stream(MemberAccountField.values())
            .map(MemberAccountField::withTableAlias)
            .collect(joining(", "));

    public String withTableAlias() {
        String name = name();
        StringBuilder fieldNameBuilder = new StringBuilder(commonName());
        if (ID.name().equals(name)) {
            fieldNameBuilder.append(" as ACCOUNT_ID");
        }
        return fieldNameBuilder.toString();
    }

    @Override
    public String filteringName() {
        return commonName();
    }

    private String commonName() {
        return MEMBER_ACCOUNT_ALIAS.toString() + "." + name();
    }
}
