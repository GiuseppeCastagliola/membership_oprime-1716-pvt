package com.odigeo.membership.enums.db;

import java.util.Arrays;

import static com.odigeo.membership.enums.db.FieldName.TableAlias.MEMBER_STATUS_ACTION_ALIAS;
import static java.util.stream.Collectors.joining;

public enum MemberStatusActionField implements FieldName {
    ID, MEMBER_ID, ACTION_TYPE, ACTION_DATE;

    public static final String MEMBER_STATUS_ACTION_FIELDS = Arrays.stream(MemberStatusActionField.values())
            .map(MemberStatusActionField::withTableAlias)
            .collect(joining(", "));

    @Override
    public String withTableAlias() {
        String fieldWithTableAlias = commonName();
        if (ID.name().equals(name())) {
            return fieldWithTableAlias + " as MEMBER_STATUS_ACTION_ID ";
        }
        return fieldWithTableAlias;
    }

    @Override
    public String filteringName() {
        return commonName();
    }

    private String commonName() {
        return MEMBER_STATUS_ACTION_ALIAS + "." + name();
    }
}
