package com.odigeo.membership;

import com.odigeo.bookingapi.v14.responses.Money;

import java.io.Serializable;

public class PrimeBookingInformation implements Serializable {

    private long bookingId;
    private Money price;
    private Money discount;

    public long getBookingId() {
        return bookingId;
    }

    public PrimeBookingInformation setBookingId(long bookingId) {
        this.bookingId = bookingId;
        return this;
    }

    public Money getPrice() {
        return price;
    }

    public PrimeBookingInformation setPrice(Money price) {
        this.price = price;
        return this;
    }


    public Money getDiscount() {
        return discount;
    }

    public PrimeBookingInformation setDiscount(Money discount) {
        this.discount = discount;
        return this;
    }
}
