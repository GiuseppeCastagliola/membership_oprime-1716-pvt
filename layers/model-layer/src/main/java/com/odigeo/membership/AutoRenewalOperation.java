package com.odigeo.membership;

public enum AutoRenewalOperation {
    DISABLE_AUTO_RENEW, ENABLE_AUTO_RENEW
}
