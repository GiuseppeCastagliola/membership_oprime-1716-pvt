package com.odigeo.membership.parameters.search;

import com.odigeo.membership.enums.db.Condition;
import com.odigeo.membership.enums.db.FieldName;
import com.odigeo.membership.enums.db.MemberAccountField;
import com.odigeo.membership.enums.db.MemberStatusActionField;
import com.odigeo.membership.enums.db.MembershipField;
import com.odigeo.membership.enums.db.MembershipRecurringField;

import java.util.List;

import static java.util.Objects.nonNull;

public abstract class SearchQueries implements Search {
    private static final String COMMA = ", ";
    private static final String GE_MEMBERSHIP_TABLE = " GE_MEMBERSHIP m ";
    private static final String GE_MEMBER_ACCOUNT_TABLE = " GE_MEMBER_ACCOUNT a ";
    private static final String JOIN_STATUS_ACTIONS = SqlKeywords.LEFT_OUTER_JOIN + " GE_MEMBER_STATUS_ACTION gmsa ON m.ID = gmsa.MEMBER_ID ";
    private static final String JOIN_RECURRING_TABLE = SqlKeywords.LEFT_OUTER_JOIN + "GE_MEMBERSHIP_RECURRING gmr ON gmr.MEMBERSHIP_ID = m.ID ";
    private static final String JOIN_RML_TABLE = SqlKeywords.LEFT_OUTER_JOIN + "GE_MEMBERSHIP_REMIND_ME_LATER gmrml ON gmrml.MEMBERSHIP_ID = m.ID ";
    private static final String JOIN_ACCOUNT = SqlKeywords.JOIN + " GE_MEMBER_ACCOUNT a ON a.ID = m.MEMBER_ACCOUNT_ID ";
    private static final String MEMBERSHIP_FIELDS_WITH_RECURRING_ID = MembershipRecurringField.RECURRING_ID.withTableAlias() + COMMA + MembershipField.MEMBERSHIP_FIELDS + COMMA + "gmrml.REMIND_ME_LATER_FLAG, gmrml.REMIND_ME_LATER_LAST_UPDATE";

    protected static final String SORT_BY_EXPDATE = "ORDER BY m.EXPIRATION_DATE DESC";
    protected static final String SEARCH_MEMBERSHIPS = SqlKeywords.SELECT + MEMBERSHIP_FIELDS_WITH_RECURRING_ID
            + SqlKeywords.FROM + GE_MEMBERSHIP_TABLE + JOIN_RECURRING_TABLE + JOIN_RML_TABLE;

    protected static final String SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS = SqlKeywords.SELECT + MEMBERSHIP_FIELDS_WITH_RECURRING_ID
            + COMMA + MemberStatusActionField.MEMBER_STATUS_ACTION_FIELDS
            + SqlKeywords.FROM + GE_MEMBERSHIP_TABLE + JOIN_RECURRING_TABLE + JOIN_RML_TABLE
            + JOIN_STATUS_ACTIONS;

    protected static final String SEARCH_MEMBERSHIPS_WITH_ACCOUNT = SqlKeywords.SELECT + MEMBERSHIP_FIELDS_WITH_RECURRING_ID
            + COMMA + MemberAccountField.MEMBER_ACCOUNT_FIELDS
            + SqlKeywords.FROM + GE_MEMBERSHIP_TABLE + JOIN_RECURRING_TABLE + JOIN_RML_TABLE
            + JOIN_ACCOUNT;

    protected static final String SEARCH_MEMBERSHIPS_WITH_STATUS_ACTIONS_AND_ACCOUNT = SqlKeywords.SELECT + MEMBERSHIP_FIELDS_WITH_RECURRING_ID
            + COMMA + MemberStatusActionField.MEMBER_STATUS_ACTION_FIELDS
            + COMMA + MemberAccountField.MEMBER_ACCOUNT_FIELDS
            + SqlKeywords.FROM + GE_MEMBERSHIP_TABLE + JOIN_RECURRING_TABLE + JOIN_RML_TABLE
            + JOIN_STATUS_ACTIONS
            + JOIN_ACCOUNT;

    protected static final String SEARCH_ACCOUNTS = SqlKeywords.SELECT + MemberAccountField.MEMBER_ACCOUNT_FIELDS
            + SqlKeywords.FROM + GE_MEMBER_ACCOUNT_TABLE;


    void addEqualsIfNotNull(List<FilterCondition> filterConditions, FieldName field, Object value) {
        if (nonNull(value)) {
            filterConditions.add(new FilterCondition(field, value));
        }
    }

    void addConditionIfNotNull(List<FilterCondition> filterConditions, FieldName field, Condition condition, Object value) {
        if (nonNull(value)) {
            filterConditions.add(new FilterCondition(field, condition, value));
        }
    }
}
