package com.odigeo.membership;

import java.math.BigDecimal;

public class MembershipPrices {
    private final BigDecimal totalPrice;
    private final BigDecimal renewalPrice;
    private final String currencyCode;

    public MembershipPrices(MembershipPricesBuilder membershipPricesBuilder) {
        totalPrice = membershipPricesBuilder.totalPrice;
        currencyCode = membershipPricesBuilder.currencyCode;
        renewalPrice = membershipPricesBuilder.renewalPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public BigDecimal getRenewalPrice() {
        return renewalPrice;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public static MembershipPricesBuilder builder() {
        return new MembershipPricesBuilder();
    }

    @SuppressWarnings("PMD.AccessorClassGeneration")
    public static final class MembershipPricesBuilder {
        private BigDecimal totalPrice;
        private BigDecimal renewalPrice;
        private String currencyCode;

        public MembershipPricesBuilder totalPrice(BigDecimal totalPrice) {
            this.totalPrice = totalPrice;
            return this;
        }

        public MembershipPricesBuilder renewalPrice(BigDecimal renewalPrice) {
            this.renewalPrice = renewalPrice;
            return this;
        }

        public MembershipPricesBuilder currencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
            return this;
        }

        public MembershipPrices build() {
            return new MembershipPrices(this);
        }
    }
}
