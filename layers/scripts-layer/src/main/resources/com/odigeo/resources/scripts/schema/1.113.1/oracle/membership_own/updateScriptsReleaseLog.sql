UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.113.1' where ID ='keep-517/01_create_table_onboarding.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.113.1' where ID ='keep-517/02_create_sequence_onboarding.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.113.1' where ID ='rollback/R_01_create_table_onboarding.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.113.1' where ID ='rollback/R_02_create_sequence_onboarding.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.113.1',systimestamp,'membership');
