package com.odigeo.membership.mocks.feesapi;

import com.odigeo.fees.exception.FeesServiceException;
import com.odigeo.fees.model.AbstractFee;
import com.odigeo.fees.model.BookingPromoCampaign;
import com.odigeo.fees.model.FeeContainer;
import com.odigeo.fees.rest.FeesService;
import com.odigeo.fees.rest.requests.CreateContainerRequest;
import com.odigeo.fees.rest.requests.DeleteFeesRequest;
import com.odigeo.fees.rest.requests.ResetFeeRequest;
import com.odigeo.fees.rest.requests.SaveFeeContainerRequest;
import com.odigeo.fees.rest.requests.UpdateContainerRequest;
import com.odigeo.fees.rest.requests.UpdateFeeContainerRequest;
import com.odigeo.fees.rest.responses.FeeInfoResponse;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class FeesServiceMock implements FeesService {

    private static final String MOCK_NOT_IMPLEMENTED = "Mock FeesService endpoint not implemented";
    private static final String NON_NULL_ID = "the FeeContainer to create must not have id";
    private final Map<Long, FeeContainer> feeContainersById = new HashMap<>();
    private final AtomicLong ids = new AtomicLong();

    public FeeContainer addFeeContainer(final FeeContainer feeContainer, final Long id) {
        Long newId = id;
        if (Objects.isNull(id)) {
            newId = ids.incrementAndGet();
            feeContainer.setId(newId);
        }
        feeContainersById.put(newId, feeContainer);
        return feeContainer;
    }

    public FeeContainer getFeeContainer(long id) {
        return feeContainersById.get(id);
    }

    @Override
    public FeeContainer saveFeeContainer(SaveFeeContainerRequest saveFeeContainerRequest) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public List<AbstractFee> updateFeeContainer(UpdateFeeContainerRequest updateFeeContainerRequest) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public void resetFee(ResetFeeRequest resetFeeRequest) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FeeContainer findFeeContainer(long id) throws FeesServiceException {
        return getFeeContainer(id);
    }

    @Override
    public List<FeeContainer> findFeeContainerByBookingId(long l) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public List<BookingPromoCampaign> findBookingPromotionalCampaignByBookingId(long l) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FeeInfoResponse getFeeInfoByFeeCode(String s, Date date) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FeeInfoResponse getFeeInfoByContainerAndLabel(String s, String s1, Date date) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FeeInfoResponse getFeeInfoByCategoryAndLabel(String s, String s1, Date date) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FeeContainer createContainer(CreateContainerRequest createContainerRequest) throws FeesServiceException {
        if (Objects.nonNull(createContainerRequest.getFeeContainer().getId())) {
            throw new FeesServiceException(NON_NULL_ID);
        }
        return addFeeContainer(createContainerRequest.getFeeContainer(), null);
    }

    @Override
    public List<AbstractFee> updateContainer(Long aLong, UpdateContainerRequest updateContainerRequest) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public void deleteFees(DeleteFeesRequest deleteFeesRequest) throws FeesServiceException {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }
}
