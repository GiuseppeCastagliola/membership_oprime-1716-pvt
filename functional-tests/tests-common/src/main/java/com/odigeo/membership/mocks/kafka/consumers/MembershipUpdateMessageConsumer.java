package com.odigeo.membership.mocks.kafka.consumers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.mocks.kafka.processors.MembershipUpdateMessageProcessor;
import com.odigeo.membership.v1.messages.MembershipUpdateMessage;

@Singleton
public class MembershipUpdateMessageConsumer extends AbstractKafkaMessageConsumer<MembershipUpdateMessage> {
    private static final String TOPIC_NAME = "MEMBERSHIP_UPDATES_V1";
    private static final String GROUP_ID = "UpdateMessageFunctionalTest";
    private final MembershipUpdateMessageProcessor messageProcessor;

    @Inject
    public MembershipUpdateMessageConsumer(MembershipUpdateMessageProcessor membershipUpdateMessageProcessor) {
        super(membershipUpdateMessageProcessor, MembershipUpdateMessage.class, TOPIC_NAME, GROUP_ID);
        this.messageProcessor = membershipUpdateMessageProcessor;
    }

    @Override
    public MembershipUpdateMessageProcessor getMessageProcessor() {
        return messageProcessor;
    }
}
