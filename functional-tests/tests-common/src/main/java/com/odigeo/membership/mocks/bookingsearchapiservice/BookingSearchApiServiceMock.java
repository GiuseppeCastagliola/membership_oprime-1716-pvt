package com.odigeo.membership.mocks.bookingsearchapiservice;

import com.odigeo.bookingsearchapi.client.v1.BookingSearchApiServiceMockImpl;
import com.odigeo.bookingsearchapi.v1.requests.SearchBookingsRequest;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.bookingsearchapi.v1.responses.SearchBookingsPageResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BookingSearchApiServiceMock extends BookingSearchApiServiceMockImpl {

    private List<BookingSummary> bookingSummaries;

    public BookingSearchApiServiceMock() {
        bookingSummaries = new ArrayList<>();
    }

    @Override
    public SearchBookingsPageResponse<List<BookingSummary>> searchBookings(String user, String password, Locale locale, SearchBookingsRequest request) {
        SearchBookingsPageResponse<List<BookingSummary>> searchBookingsPageResponse = new SearchBookingsPageResponse<>();
        List<BookingSummary> bookingSummariesByMembershipId = bookingSummaries.stream()
                .filter(getSearchBookingSummaryPredicate(request))
                .collect(Collectors.toList());
        searchBookingsPageResponse.setBookings(bookingSummariesByMembershipId);
        return searchBookingsPageResponse;
    }

    private Predicate<BookingSummary> getSearchBookingSummaryPredicate(SearchBookingsRequest request) {
        return bookingSummary -> bookingSummary.getMembershipId().equals(request.getMembershipId())
                && (Objects.isNull(request.getIsBookingSubscriptionPrime())
                || request.getIsBookingSubscriptionPrime().equalsIgnoreCase(bookingSummary.getIsBookingSubscriptionPrime()));
    }

    public void addBookingSummary(BookingSummary bookingSummary) {
        if (bookingSummaries == null) {
            bookingSummaries = new ArrayList<>();
        }
        bookingSummaries.add(bookingSummary);
    }

    public void resetMocks() {
        bookingSummaries.clear();
    }
}
