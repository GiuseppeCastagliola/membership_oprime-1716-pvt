package com.odigeo.membership.mocks.kafka;

import com.google.inject.Inject;
import com.odigeo.membership.mocks.kafka.consumers.MembershipSubscriptionMessageConsumer;
import com.odigeo.membership.mocks.kafka.consumers.MembershipUpdateMessageConsumer;
import com.odigeo.membership.mocks.kafka.consumers.WelcomeToPrimeMessageConsumer;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.log4j.Logger;

@ScenarioScoped
public class MessageControllerWorld {
    private static final Logger LOGGER = Logger.getLogger(MessageControllerWorld.class);

    private final MembershipSubscriptionMessageConsumer membershipSubscriptionMessageConsumer;
    private final MembershipUpdateMessageConsumer membershipUpdateMessageConsumer;
    private final WelcomeToPrimeMessageConsumer welcomeToPrimeMessageConsumer;

    @Inject
    public MessageControllerWorld(MembershipSubscriptionMessageConsumer membershipSubscriptionMessageConsumer,
                                  MembershipUpdateMessageConsumer membershipUpdateMessageConsumer,
                                  WelcomeToPrimeMessageConsumer welcomeToPrimeMessageConsumer) {
        this.membershipSubscriptionMessageConsumer = membershipSubscriptionMessageConsumer;
        this.membershipUpdateMessageConsumer = membershipUpdateMessageConsumer;
        this.welcomeToPrimeMessageConsumer = welcomeToPrimeMessageConsumer;
    }

    public MembershipSubscriptionMessageConsumer getMembershipSubscriptionMessageConsumer() {
        return membershipSubscriptionMessageConsumer;
    }

    public MembershipUpdateMessageConsumer getMembershipUpdateMessageConsumer() {
        return membershipUpdateMessageConsumer;
    }

    public WelcomeToPrimeMessageConsumer getWelcomeToPrimeMessageConsumer() {
        return welcomeToPrimeMessageConsumer;
    }

    public void uninstall() {
        LOGGER.info("resetMessageList() for mock kafka consumers");
        membershipSubscriptionMessageConsumer.getMessageProcessor().resetMessageList();
        membershipUpdateMessageConsumer.getMessageProcessor().resetMessageList();
        welcomeToPrimeMessageConsumer.getMessageProcessor().resetMessageList();
    }
}
