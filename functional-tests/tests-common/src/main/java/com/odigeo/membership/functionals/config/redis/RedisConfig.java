package com.odigeo.membership.functionals.config.redis;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class RedisConfig {

    private static final Integer PORT = 6379;

    private String localIp;

    public String getLocalIp() {
        return localIp;
    }

    public void setLocalIp(String localIp) {
        this.localIp = localIp;
    }

    public Integer getPort() {
        return PORT;
    }
}
