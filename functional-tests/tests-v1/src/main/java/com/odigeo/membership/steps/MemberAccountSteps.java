package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.converter.BooleanConverter;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.functionals.membership.CheckMemberOnPassengerListRequestBuilder;
import com.odigeo.membership.functionals.membership.MembershipAccountUpdateRequestBuilder;
import com.odigeo.membership.functionals.membership.TravellerParametersContainerBuilder;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.request.product.UpdateMemberAccountRequest;
import com.odigeo.membership.response.MembershipAccountInfo;
import com.odigeo.membership.world.MemberAccountManagementWorld;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.DataTable;
import cucumber.api.Transform;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static org.testng.Assert.assertEquals;

@ScenarioScoped
public class MemberAccountSteps extends CommonSteps {

    private final MembershipManagementWorld world;
    private final MemberAccountManagementWorld memberAccountWorld;
    private static final String NULL_VALUE = "null";

    @Inject
    public MemberAccountSteps(ServerConfiguration serverConfiguration, MembershipManagementWorld world, final MemberAccountManagementWorld memberAccountWorld) {
        super(serverConfiguration);
        this.world = world;
        this.memberAccountWorld = memberAccountWorld;
    }

    @When("^the client request subscription details to membership Api with memberAccountId (\\d+)$")
    public void requestAllSubscriptionDetails(long memberId) throws InvalidParametersException {
        world.setSubscriptionsDetails(userAreaService.getAllMemberSubscriptionsDetails(memberId));
    }

    @When("^the client request updateMembership to membership with memberAccountId (\\w+), name (\\w+) and lastNames (\\w+)$")
    public void theClientRequestUpdateMembershipToMembershipAPI(String memberAccountId, String name, String lastNames) throws InvalidParametersException {
        world.setMemberAccountUpdateRequest(memberAccountId, name, lastNames);
        world.setUpdatedMemberId(membershipService.updateMemberAccount(world.getMemberAccountRequest()));
    }

    @When("^the user wants to update the MemberAccount:$")
    public void whenUpdateMemberAccountRequest(List<MembershipAccountUpdateRequestBuilder> membershipAccountUpdateRequestTable) {
        for (MembershipAccountUpdateRequestBuilder membershipAccountUpdateRequestBuilder : Objects.requireNonNull(membershipAccountUpdateRequestTable)) {
            updateMemberAccount(membershipAccountUpdateRequestBuilder);
        }
    }

    @When("^the client asks if the user (\\d+) has any active membership for the brand (\\w+)$")
    public void whenAskIfUserHasAnyActiveMembershipsForTheBrand(long userId, String brand) {
        world.setUserHasAnyMembershipForBrand(userAreaService.userHasMembershipForBrand(userId, brand));
    }

    @Then("^user has any active membership for the brand is (true|false)$")
    public void checkUserHasAnyActiveMembershipsForTheBrand(@Transform(BooleanConverter.class) Boolean result) {
        assertEquals(world.getUserHasAnyMembershipForBrand(), result.booleanValue());
    }

    @When("^the client asks for membership account ID with the membership ID (\\d+)$")
    public void whenAsksForMembershipAccountIdWithMembershipId(long membershipId) {
        world.setMembershipAccountId(userAreaService.getMembershipAccountId(membershipId));
    }

    @Then("^membership account ID is (\\d+)$")
    public void membershipAccountIdIs(Long membershipAccountId) {
        assertEquals(world.getMembershipAccountId(), membershipAccountId);
    }

    @When("^the client request getMembershipAccount to membership API with membershipAccountId (\\d+)$")
    public void whenClientRequestMembershipAccountWithMembershipAccountId(long membershipAccountId) {
        memberAccountWorld.setMembershipAccountInfo(userAreaService.getMembershipAccount(membershipAccountId));
    }

    @When("^the following eligibleForFreeTrialRequest is made:$")
    public void anEligibleForFreeTrialRequestIsMade(DataTable requestDataTable) {
        FreeTrialCandidateRequest freeTrialCandidateRequest = convertToFreeTrialCandidateRequest(requestDataTable);
        memberAccountWorld.setEligibleForFreeTrialResponse(membershipService.isEligibleForFreeTrial(freeTrialCandidateRequest));
    }

    @Then("^the membershipAccount response is:$")
    public void thenMembershipAccountResponse(List<MembershipAccountInfo> expectedAccounts) {
        MembershipAccountInfo responseMembership = memberAccountWorld.getMembershipAccountInfo();
        for (MembershipAccountInfo account : expectedAccounts) {
            assertEquals(responseMembership.getId(), account.getId());
            assertEquals(responseMembership.getUserId(), account.getUserId());
            assertEquals(responseMembership.getFirstName(), account.getFirstName());
            assertEquals(responseMembership.getLastName(), account.getLastName());
            assertEquals(responseMembership.getTimestamp(), account.getTimestamp());
        }
    }

    @When("^requested to apply membership with checkMemberOnPassengerListRequest:$")
    public void requestToApplyMembershipWithCheckMemberOnPassengerListRequest() {
        memberAccountWorld.setMembershipApplicable(membershipService.applyMembership(memberAccountWorld.getCheckMemberOnPassengerListRequest()));
    }

    @Then("^the membership (.*?) applicable for this passengers list:$")
    public void thenMembershipApplicableForPassengerList(String success) {
        assertEquals(memberAccountWorld.getMembershipApplicable(), Boolean.valueOf("is".equals(success)));
    }

    @Then("^the returned eligibility for free prime should be (true|false)$")
    public void theReturnedEligibilityForFreePrime(String freePrimeEligibility) {
        boolean expectedEligibilityResponse = Boolean.parseBoolean(freePrimeEligibility);
        boolean eligibleForFreePrimeResponse = memberAccountWorld.getEligibleForFreeTrialResponse();

        assertEquals(expectedEligibilityResponse, eligibleForFreePrimeResponse);
    }

    @Given("^the next checkMemberOnPassengerListRequest:$")
    public void theNextCheckMemberPassengerListRequest(
            List<CheckMemberOnPassengerListRequestBuilder> checkMemberOnPassengerListRequestBuilderList) {
        memberAccountWorld.setCheckMemberOnPassengerListRequest(checkMemberOnPassengerListRequestBuilderList.get(0));
    }

    @And("^the next checkMemberOnPassengerListRequest includes the following passengers:$")
    public void addPassengersToCheckMemberOnPassengerList(List<TravellerParametersContainerBuilder> travellerParametersContainerBuilderList) {
        memberAccountWorld.addPassengersToCheckMemberOnPassengerList(travellerParametersContainerBuilderList);
    }

    @When("^the client request createMembership to membership API$")
    public void theClientRequestCreateMembershipToMembershipAPI() {
        world.setCreatedMemberId(membershipService.createMembership(memberAccountWorld.getCreateMembershipRequest()));
    }

    private void updateMemberAccount(MembershipAccountUpdateRequestBuilder membershipAccountUpdateRequest) {
        UpdateMemberAccountRequest updateMemberAccountRequest = new UpdateMemberAccountRequest();
        updateMemberAccountRequest.setMemberAccountId(membershipAccountUpdateRequest.getMemberAccountId());
        updateMemberAccountRequest.setUserId(membershipAccountUpdateRequest.getUserId());
        updateMemberAccountRequest.setName(membershipAccountUpdateRequest.getName());
        updateMemberAccountRequest.setLastNames(membershipAccountUpdateRequest.getLastNames());
        updateMemberAccountRequest.setOperation(membershipAccountUpdateRequest.getOperation());
        membershipService.updateMemberAccount(updateMemberAccountRequest);
    }

    private FreeTrialCandidateRequest convertToFreeTrialCandidateRequest(DataTable requestDataTable) {
        Map<String, String> asMapMutable = new HashMap<>(requestDataTable.asMap(String.class, String.class));
        asMapMutable.keySet().forEach(k -> asMapMutable.replace(k, NULL_VALUE, null));

        return new FreeTrialCandidateRequest.Builder()
                .withEmail(asMapMutable.get("email"))
                .withWebsite(asMapMutable.get("website"))
                .withName(asMapMutable.get("name"))
                .withLastNames(asMapMutable.get("lastNames"))
                .build();
    }
}
