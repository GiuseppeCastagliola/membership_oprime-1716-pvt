package com.odigeo.membership.world;

import com.odigeo.membership.response.BlackListedPaymentMethod;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

@ScenarioScoped
public class BlacklistedPaymentMethodsWorld {

    private List<BlackListedPaymentMethod> blackListedPaymentMethods;

    public List<BlackListedPaymentMethod> getBlackListedPaymentMethods() {
        return blackListedPaymentMethods;
    }

    public void setBlackListedPaymentMethods(List<BlackListedPaymentMethod> blackListedPaymentMethods) {
        this.blackListedPaymentMethods = blackListedPaymentMethods;
    }
}
