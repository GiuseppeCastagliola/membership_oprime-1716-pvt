package com.odigeo.membership.world;

import com.odigeo.product.response.BookingApiMembershipInfo;
import com.odigeo.product.v2.model.Product;
import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class MembershipProductManagementWorld {

    private Product product;
    private BookingApiMembershipInfo bookingApiMembershipInfo;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BookingApiMembershipInfo getBookingApiMembershipInfo() {
        return bookingApiMembershipInfo;
    }

    public void setProductBookingApiInfo(BookingApiMembershipInfo bookingApiProductInfo) {
        this.bookingApiMembershipInfo = bookingApiProductInfo;
    }
}
