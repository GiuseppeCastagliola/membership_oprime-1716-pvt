package com.odigeo.membership.functionals.config;


import bean.test.BeanTest;

public class KafkaConfigTest extends BeanTest<KafkaConfig> {

    @Override
    protected KafkaConfig getBean() {
        KafkaConfig kafkaConfig = new KafkaConfig();
        kafkaConfig.setLocalIp("10.1.1.1");
        return kafkaConfig;
    }
}