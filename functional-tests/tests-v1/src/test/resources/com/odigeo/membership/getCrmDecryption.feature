Feature: Test getCrmDecryption

  Scenario Outline: Checking CRM token decryption
    When Request CRM token decryption with token <token>
    Then Result matches the expected string <decryptedToken>

    Examples:
      | token                                                                                            | decryptedToken                                            |
      | a0quclh9BARlNM1bmuKS492y0MwxXA7rBM2ZEJVuISQ%3d                                                   | _adrian_93@hotmail.esAdrian                               |
      | 9c8p1DkUAJswBqtM6WxRivyzFJu7k7Har5zg%2FQ6KyEbMByFomylu4jj%2F5jFYxIecQ1ajgjdRoGKcB8Wq3UcBZg%3D%3D | bookingId=4962933454&email=leonardo.rey@edreamsodigeo.com |

  Scenario: Check if error occurs for invalid token
    When Request CRM token decryption with token invalid
    Then Exception occured
