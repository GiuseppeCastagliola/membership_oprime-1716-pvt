Feature: Test createMembership service

  Scenario: create membership pendingToCollect
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 1234            | 3436   | TEST      | TEST      |
    Given the next membership stored in db:
      | memberId | status  | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | 44698    | EXPIRED | 44.99   | 2018-05-04     | 2019-05-04     | 1234            | ENABLED     |
    Given the next createMembershipPendingToCollectRequest:
      | website | monthsToRenewal | sourceType     | membershipType | memberAccountId | expirationDate          | currencyCode | recurringId | subscriptionPrice |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 1234            | 2023-02-04T00:00:00.000 | EUR          | xyz         | 49.99             |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_COLLECT and a valid fee container ID
    And all information for a pending to collect is filled in the membership
    And the recurringId xyz has been inserted for the new membership
    And the membership fee has been inserted with right information

  Scenario: create membership pendingToCollect returns the membershipId that already exists for the same website
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 1234            | 3436   | TEST      | TEST      |
    Given the next membership stored in db:
      | memberId | website | status             | balance | activationDate | expirationDate | memberAccountId | autoRenewal |
      | 44698    | ES      | EXPIRED            | 44.99   | 2018-05-04     | 2019-05-04     | 1234            | ENABLED     |
      | 48579    | ES      | PENDING_TO_COLLECT | 44.99   | 2019-05-04     | 2020-05-04     | 1234            | ENABLED     |
    Given the next createMembershipPendingToCollectRequest:
      | website | monthsToRenewal | sourceType     | membershipType | memberAccountId | expirationDate          | currencyCode | recurringId | subscriptionPrice |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | 1234            | 2023-02-04T00:00:00.000 | EUR          | xyz         | 49.99             |
    When the client request createMembership to membership API
    Then no new membership in pending to collect was created for memberAccount 1234 and website ES
    And the membershipId returned is 48579

  Scenario Outline: CreateNewMembershipRequest with existing userId creates a pending to activate membership
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | userId   | name  | lastNames |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | <userId> | Emiri | Kaya      |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE
    And the membership belongs to a user with id <userId>
    Examples:
      | userId |
      | 7395   |

  Scenario: CreateNewMembershipRequest with user creation info
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | name  | lastNames | email              | locale | trafficInterfaceId |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | Emiri | Kaya      | ekaya@thy.com      | tr_TR  | 2                  |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | Burak | Tabak     | registered@thy.com | tr_TR  | 1                  |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE

  Scenario Outline: In CreateNewMembershipRequest userId has priority over user creation info
    Given the following createNewMembershipRequest:
      | website | monthsToRenewal | sourceType     | membershipType | name  | lastNames | email       | locale | trafficInterfaceId | userId   |
      | ES      | 12              | FUNNEL_BOOKING | BASIC          | Talat | Tata      | any@thy.com | tr_TR  | 2                  | <userId> |
    When the client request createMembership to membership API
    Then a membership is created with the status PENDING_TO_ACTIVATE
    And the membership belongs to a user with id <userId>
    Examples:
      | userId |
      | 112    |

  Scenario Outline: Create Basic FREE membership with existing user
    Given the following createBasicFreeMembershipRequest:
      | website | monthsToRenewal | sourceType   | membershipType | currencyCode | name | lastNames | userId   | channel   |
      | ES      | 6               | POST_BOOKING | BASIC_FREE     | EUR          | Juan | Sanchez   | <userId> | <channel> |
    When the client request createMembership to membership API
    Then a membership is created with the status ACTIVATED
    And membership is created with autorenewal disabled
    And the membership belongs to a user with id <userId>

    Examples:
      | userId  | channel      |
      | 1234567 | LANDING_PAGE |
      | 7654321 | ANDROID      |

  Scenario: Create Basic FREE membership with user creation info
    Given the following createBasicFreeMembershipRequest:
      | website | monthsToRenewal | sourceType   | membershipType | currencyCode | name | lastNames | email        | locale | trafficInterfaceId | channel |
      | ES      | 6               | POST_BOOKING | BASIC_FREE     | EUR          | Paul | Winter    | paul@yes.com | es_ES  | 1                  | IOS     |
    When the client request createMembership to membership API
    Then a membership is created with the status ACTIVATED
    And membership is created with autorenewal disabled
