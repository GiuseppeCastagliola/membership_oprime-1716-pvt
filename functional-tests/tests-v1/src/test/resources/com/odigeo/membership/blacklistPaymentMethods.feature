Feature: Test endpoints related to blacklisting payment methods

  Background:
    Given the following blacklisted payment methods are stored in the db:
      | id    | errorMessage                           | errorType  | timestamp  | membershipId |
      | 1087  | COLLECTION_LOST_CARD                   | type1      | 2020-04-07 | 17           |
      | 1099  | COLLECTION_FRAUD_SUSPECT               | type2      | 2020-04-10 | 17           |
      | 1012  | COLLECTION_NOT_PERMITTED_TO_CARDHOLDER | type3      | 2020-04-20 | 19           |

  Scenario Outline: getBlacklistedPaymentMethods returns all blacklisted payment methods of the membership
    When requested getBlacklistedPaymentMethods with membershipId <membershipId>
    Then the resulting blacklisted payment methods list contains <listSize> items
    Examples:
      | membershipId | listSize |
      | 17           | 2        |
      | 66           | 0        |

  Scenario Outline: addToBlacklist adds new payment methods to the blacklisted ones
    When called addToBlacklist payment methods for membershipId <membershipId>
    | id   | timestamp    | errorType | errorMessage                           | membershipId   |
    | 1033 | 2020-04-22   | type3     | COLLECTION_NOT_PERMITTED_TO_CARDHOLDER | <membershipId> |
    | 1035 | 2020-04-23   | type2     | COLLECTION_FRAUD_SUSPECT               | <membershipId> |
    And requested getBlacklistedPaymentMethods with membershipId <membershipId>
    Then the resulting blacklisted payment methods list contains <listSize> items
    Examples:
      | membershipId | listSize |
      | 17           | 4        |
      | 66           | 2        |