Feature: Test getMembershipProduct and getMembershipProductDetails service

  Background:
    Given the next membership product stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration | productStatus | feeContainerId   |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             | CONTRACT      | null             |
      | 345      | ES          | PENDING_TO_COLLECT  | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 66.6    | FUNNEL_BOOKING | 12             | CONTRACT      | 222222           |
    And the next membership product fee stored in db:
      |membershipId | amount | currency | feeType            |
      | 123         | 20.31  | EUR      | MEMBERSHIP_RENEWAL |
    And the fees-api has the following fee containers:
      | Id     | FeeContainerType   | Amount | FeeLabel   | Currency | SubCode |
      | 222222 | MEMBERSHIP_RENEWAL | 67.6   | MARKUP_TAX | EUR      | AE10    |

  Scenario: Checking getProduct service
    When the product information is requested for the product id 123
    Then the response to the membership product request is:
      | productId | amount |
      | 123       | 20.31  |

  Scenario: Checking getProductDetails service
    When the product information details is requested for the product id 123
    Then the response to the membership product request is:
      | productId | amount |
      | 123       | 20.31  |

  Scenario: Checking getProduct service with feeContainerId
    When the product information is requested for the product id 345
    Then the response to the membership product request is:
      | productId | amount |
      | 345       | 67.6   |

  Scenario: Checking getProductDetails service with feeContainerId
    When the product information details is requested for the product id 345
    Then the response to the membership product request is:
      | productId | amount |
      | 345       | 67.6   |
